c**********************************************************************
c     Copyright (c) 2020 Tomas Manik
c
c     Permission is hereby granted, free of charge, to any person 
c     obtaining a copy of this software and associated documentation 
c     files (the "Software"), to deal in the Software without 
c     restriction, including without limitation the rights to use, 
c     copy, modify, merge, publish, distribute, sublicense, and/or 
c     sell copies of the Software, and to permit persons to whom the 
c     Software is furnished to do so, subject to the following 
c     conditions:
c
c     The above copyright notice and this permission notice shall be 
c     included in all copies or substantial portions of the Software.
c
c     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
c     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
c     OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
c     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
c     HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
c     WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
c     FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
c     OTHER DEALINGS IN THE SOFTWARE.
c
c     Contributors: Tomas Manik (tomas.manik@ntnu.no)
c                   Hassan Moradi Asadkandi (hassan.m.asadkandi@ntnu.no)
c                   Bjorn Holmedal (bjorn.holmedal@ntnu.no)
c
c     Reference as "T. M�nik, H.M. Asadkandi, B. Holmedal. A robust 
c     algorithm for rate-independent crystal plasticity, Computer 
c     Methods in Applied Mechanics and Engineering, Volume 393. 
c     DOI https://doi.org/10.1016/j.cma.2022.114831"
c
c
c
      subroutine umat(stress, statev, ddsdde, sse, spd, scd,
     1 rpl, ddsddt, drplde, drpldt,
     2 stran, dstran, time, dtime, temp, dtemp, predef, dpred, cmname,
     3 ndi, nshr, ntens, nstatv, props, nprops, coords, drot, pnewdt,
     4 celent, dfgrd0, dfgrd1, noel, npt, layer, kspt, jstep, kinc)
c     
      include 'aba_param.inc'
c     
      character*80 cmname
      dimension stress(ntens), statev(nstatv),
     1 ddsdde(ntens, ntens), ddsddt(ntens), drplde(ntens),
     2 stran(ntens), dstran(ntens), time(2), predef(1), dpred(1),
     3 props(nprops), coords(3), drot(3,3), dfgrd0(3,3), dfgrd1(3,3),
     4 jstep(4)
c
c
c
c     user coding to define ddsdde, stress, statev, sse, spd, scd
c     and, if necessary, rpl, ddsddt, drplde, drpldt, pnewdt
c
c**********************************************************************
c     This is a user-defined material subroutine UMAT that: 
c      - can be used to define the mechanical constitutive behavior of 
c        a material.
c      - will be called at all material calculation points of elements 
c        for which the material definition includes a user-defined 
c        material behavior
c      - can be used with any procedure that includes mechanical 
c        behavior
c      - can use solution-dependent state variables
c      - must update the stresses and solution-dependent state variables
c        to their values at the end of the increment for which it is 
c        called
c      - must provide the material Jacobian matrix DDSDDE for the 
c        mechanical constitutive model
c      - can be used in conjunction with user subroutine USDFLD to 
c        redefine any field variables before they are passed in.  
c      
c     This implementation of UMAT includes:
c      - crystal plasticity in a hypo-elastic framework with regularized
c      - single crystal yield surface
c      - elastic stiffness modulus with cubic symmetry
c      - general hardening law with interaction matrix for self- and latent
c      - implicit backward-Euler return map algorithm with radial return
c        initial guess and line-search
c      - natural vector and matrix notation for 2nd and 4th order tensors
c
c**********************************************************************
c (OUT) DDSDDE is an array, dimension (NTENS,NTENS)
c     Jacobian matrix of the constitutive model. DDSDDE(I,J) defines the 
c     change in the Ith stress component at the end of the time 
c     increment caused by an infinitesimal perturbation of the Jth 
c     component of the strain increment array. Unless you invoke the 
c     unsymmetric equation solution capability for the user-defined 
c     material, ABAQUS/Standard will use only the symmetric part of 
c     DDSDDE. The symmetric part of the matrix is calculated by taking 
c     one half the sum of the matrix and its transpose. For viscoelastic 
c     behavior in the frequency domain, the Jacobian matrix must be 
c     dimensioned as DDSDDE(NTENS,NTENS,2). The stiffness contribution 
c     (storage modulus) must be provided in DDSDDE(NTENS,NTENS,1), 
c     while the damping contribution (loss modulus) must be provided in 
c     DDSDDE(NTENS,NTENS,2). 
c
c (IN/OUT) STRESS ia an array, dimension (NTENS)
c     This array is passed in as the stress tensor at the beginning
c     of the increment and must be updated in this routine to be the
c     stress tensor at the end of the increment. If you specified
c     initial stresses, this array will contain the initial 
c     stresses at the start of the
c     analysis. The size of this array depends on the value of NTENS
c     as defined below. In finite-strain problems the stress tensor
c     has already been rotated to account for rigid body motion in
c     the increment before UMAT is called, so that only the
c     corotational part of the stress integration should be done in 
c     UMAT. The measure of stress used is �true� (Cauchy) stress. 
c     If the UMAT utilizes a hybrid formulation that is total (as 
c     opposed to the default incremental behavior), the stress array 
c     is extended beyond NTENS. Check the Abaqus Documentation for 
c     the details. For storing of the components of a symmetric
c     stress tensor as a 1-dimensional array, Abaqus applies the 
c     Voigt convention ordered as (Sxx, Syy, Szz, Sxy, Syz, Sxz), 
c     i.e. Sxx, Syy and Szz are the normal stress components, 
c     Sxy, Syz and Sxz are the shear components. 
c
c (IN/OUT) STATEV is an array, dimension (NSTATV)
c     An array containing the solution-dependent state variables.
c     These are passed in as the values at the beginning of the
c     increment unless they are updated in user subroutines USDFLD
c     or UEXPAN, in which case the updated values are passed in. In all 
c     cases STATEV must be returned as the values at the end of the 
c     increment. The size of the array is defined as described in
c     �Allocating space� in the Abaqus Documentation. In finite-strain 
c     problems any vector-valued or tensor-valued state variables must 
c     be rotated to account for rigid body motion of the material, 
c     in addition to any update in the values associated with 
c     constitutive behavior. The rotation increment matrix, DROT, is 
c     provided for this purpose.
c
c (IN/OUT) SSE, SPD, SCD
c     Specific elastic strain energy, plastic dissipation, and
c     �creep� dissipation, respectively. These are passed in as
c     the values at the start of the increment and should be
c     updated to the corresponding specific energy values at
c     the end of the increment. They have no effect on the solution,
c     except that they are used for energy output.
c
c *** Only in a fully coupled thermal-stress analysis ***
c (OUT) RPL
c     Volumetric heat generation per unit time at the end of the 
c     increment caused by mechanical working of the material.
c
c (OUT) DDSDDT is an array, dimension (NTENS)
c     Variation of the stress increments with respect to the temperature.
c
c (OUT) DRPLDE is an array, dimension (NTENS)
c     Variation of RPL with respect to the strain increments.
c
c (OUT) DRPLDT
c     Variation of RPL with respect to the temperature.
c
c *** Variables that can be updated ***
c (OUT) "optional" PNEWDT
c     Ratio of suggested new time increment to the time increment being
c     used (DTIME, see discussion later in this section). This variable
c     allows you to provide input to the automatic time incrementation
c     algorithms in ABAQUS/Standard (if automatic time incrementation is
c     chosen). For a quasi-static procedure the automatic time stepping 
c     that ABAQUS/Standard uses, which is based on techniques for 
c     integrating standard creep laws, cannot be controlled from within
c     the UMAT subroutine. PNEWDT is set to a large value before each 
c     call to UMAT. If PNEWDT is redefined to be less than 1.0, 
c     ABAQUS/Standard must abandon the time increment and attempt it 
c     again with a smaller time increment. The suggested new time 
c     increment provided to the automatic time integration algorithms is
c     PNEWDT � DTIME, where the PNEWDT used is the minimum value
c     for all calls to user subroutines that allow redefinition of 
c     PNEWDT for this iteration. If PNEWDT is given a value that is 
c     greater than 1.0 for all calls to user subroutines for this 
c     iteration and the increment converges in this iteration, 
c     ABAQUS/Standard may increase the time increment. The suggested new
c     time increment provided to the automatic time integration 
c     algorithms is PNEWDT � DTIME, where the PNEWDT used is the minimum
c     value for all calls to user subroutines for this iteration.
c     If automatic time incrementation is not selected in the analysis 
c     procedure, values of PNEWDT that are greater than 1.0 will be 
c     ignored and values of PNEWDT that are less than 1.0 will cause the
c     job to terminate.
c
c *** Variables passed in for information ***
c (IN) STRAN is an array, dimension (NTENS)
c     An array containing the total strains at the beginning of the 
c     increment. If thermal expansion is included in the same material 
c     definition, the strains passed into UMAT are the mechanical 
c     strains only (that is, the thermal strains computed based upon 
c     the thermal expansion coefficient have been subtracted from the 
c     total strains). These strains are available for output
c     as the �elastic� strains. In finite-strain problems the strain 
c     components have been rotated to account for rigid body motion in 
c     the increment before UMAT is called and are approximations to 
c     logarithmic strain. For storing of the components of a symmetric
c     strain tensor as a 1-dimensional array, Abaqus applies the 
c     Voigt convention ordered as (Exx, Eyy, Ezz, 2*Exy, 2*Eyz, 2*Exz), 
c     i.e. Exx, Eyy and Ezz are the normal strain components, 
c     Exy, Eyz and Exz are the shear components. Note, that Abaqus 
c     always reports shear strain as engineering shear strain.
c
c (IN) DSTRAN is an array, dimension (NTENS)
c     Array of strain increments. If thermal expansion is included in 
c     the same material definition, these are the mechanical strain 
c     increments (the tota) strain increments minus the thermal strain 
c     increments). For storing of the components of a symmetric
c     strain increment tensor as a 1-dimensional array, Abaqus applies 
c     the Voigt convention ordered as 
c     (dExx, dEyy, dEzz, 2*dExy, 2*dEyz, 2*dExz), 
c     i.e. dExx, dEyy and dEzz are the normal strain increment 
c     components, dExy, dEyz and dExz are the shear increment 
c     components. Note, that Abaqus always reports shear strain 
c     increments as engineering shear strain increments.
c
c (IN) TIME(1)
c     Value of step time at the beginning of the current increment.
c
c (IN) TIME(2)
c     Value of total time at the beginning of the current increment.
c
c (IN) DTIME
c     Time increment.
c
c (IN) TEMP
c     Temperature at the start of the increment.
c
c (IN) DTEMP
c     Increment of temperature.
c
c (IN) PREDEF
c     Array of interpolated values of predefined field variables at this
c     point at the start of the increment, based on the values read in 
c     at the nodes.
c
c (IN) DPRED
c     Array of increments of predefined field variables.
c
c (IN) CMNAME
c     User-defined material name, left justified. Some internal material 
c     models are given names starting with the �ABQ_� character string. 
c     To avoid conflict, you should not use �ABQ_� as the leading string 
c     for CMNAME.
c
c (IN) NDI
c     Number of direct stress components at this point.
c
c (IN) NSHR
c     Number of engineering shear stress components at this point.
c
c (IN) NTENS
c     Size of the stress or strain component array (NDI + NSHR).
c
c (IN) NSTATV
c     Number of solution-dependent state variables that are associated 
c     with this material type (defined as described in �Allocating space� 
c     in Abaqus Documentation.
c
c (IN) PROPS is an array, dimension (NPROPS)
c     User-specified array of material constants associated with this 
c     user material.
c
c (IN) NPROPS
c     User-defined number of material constants associated with this 
c     user material.
c
c (IN) COORDS
c     An array containing the coordinates of this point. These are the 
c     current coordinates if geometric nonlinearity is accounted for 
c     during the step, otherwise, the array contains the original 
c     coordinates of the point.
c
c (IN) DROT is an array, dimensions (3,3)
c     Rotation increment matrix. This matrix represents the increment of
c     rigid body rotation of the basis system in which the components of 
c     stress (STRESS) and strain (STRAN) are stored. It is provided so 
c     that vector- or tensor-valued state variables can be rotated 
c     appropriately in this subroutine: stress and strain components are
c     already rotated by this amount before UMAT is called. This matrix 
c     is passed in as a unit matrix for small-displacement analysis and 
c     for large-displacement analysis if the basis system for the 
c     material point rotates with the material (as in a shell element or
c     when a local orientation is used).
c
c (IN) CELENT
c     Characteristic element length, which is a typical length of a line
c     across an element for a first-order element; it is half of the same
c     typical length for a second-order element. For beams and trusses 
c     it is a characteristic length along the element axis. For membranes
c     and shells it is a characteristic length in the reference surface. 
c     For axisymmetric elements it is a characteristic length in the  
c     plane only. For cohesive elements it is equal to the constitutive
c     thickness.
c
c (IN) DFGRD0 is an array, dimension (3,3)
c     Array containing the deformation gradient at the beginning of the 
c     increment. See the discussion regarding the availability of the 
c     deformation gradient for various element types.
c
c (IN) DFGRD1 is an array, dimension (3,3)
c     Array containing the deformation gradient at the end of the 
c     increment. The components of this array are set to zero if 
c     nonlinear geometric effects are not included in the step 
c     definition associated with this increment. See the discussion 
c     regarding the availability of the deformation gradient for
c     various element types.
c
c (IN) NOEL
c     Element number.
c
c (IN) NPT
c     Integration point number.
c
c (IN) LAYER
c     Layer number (for composite shells and layered solids).
c
c (IN) KSPT
c     Section point number within the current layer.
c
c (IN) KSTEP
c     Step number.
c
c (IN) KINC
c     Increment number.
c
      integer, parameter  ::  nssgr = 7,
c                             number of slip system classes
     +                        Nsmax = 200
c                             max number of slip systems in total
      integer :: i, j, k, info, Nslips, Nsg(nssgr)
      real(8) :: svec(6), dstrain(6), am(6,6), L(3,3), W(3,3), D(3,3),
     +           Q(3,3), R(3,3), Rn(3,3), Rest(3,3), DR(3,3), S(3,3),  
     +           wc(3), R5(5,5), ang(3), svec_old(6), Cdiag(6), DF(3,3),
     +           Sg(3,3), am0(5,5), DSSE, inner, vdot, Lg(3,3), 
     +           oldF(3,3), newF(3,3), DEvec(6)
      logical :: firstcall
      logical :: full = .FALSE.
      real(8) :: P(5,Nsmax), Omega(3,Nsmax)
      data       firstcall /.TRUE./
c
c ------- SOLUTION DEPENDENT STATE VARIABLES USED IN THIS UMAT ---------
c     
c     STATEV(1:3) - Euler angles phi1, PHI, phi2
c     STATEV(4)   - the accumulated plastic strain
c     STATEV(5)   - the plastic work rate
c     STATEV(6)   - number of Newton-Raphson iterations to converge
c     STATEV(7)   - the largest number of line-searches within one iter.
c     STATEV(8)   - return-map convergence status (see INFO in returnmap)
c     STATEV(9)   - the converged value of the residual PSI
c     STATEV(10:18) - rotation matrix R from global to crystal sys
c     STATEV(19)    - number of actives systems   
c     STATEV(20:25) - co-rotated Cauchy stress
c     STATEV(26:25+Nslips)          - critical resolved shear stresses 
c     STATEV(26+Nslips:25+2*Nslips) - slip rates   
c
c --------------- MATERIAL PARAMETERS USED IN THIS UMAT ----------------
c 
c     PROPS(1)  - type of returnmap
c     PROPS(2)  - C11 \
c     PROPS(3)  - C12 |>  elastic constants for cubic symmtery 
c     PROPS(4)  - C44 /
c     PROPS(5)  - exponent of the regularized single crystal yield func
c     PROPS(6)  - slip systems identificator (cs)
c     PROPS(7)  - differentiation of forward and reverse slips (if 1)
c     PROPS(8)  - flag for printing the convergence message of returnmap
c               - critical shear stresses followed by Voce hardening 
c               - terms, i.e. tauC, tauSAT1, gmSAT1, tauSAT2, gmSAT2,
c               - tauSAT3, gmSAT3, theta, per each slip system group:
c     PROPS(9:16)  - {111}<110> FCC octahedrals
c     PROPS(17:24) - {110}<110>
c     PROPS(25:32) - {100}<011>
c     PROPS(33:40) - {112}<011>
c     PROPS(41:48) - {110}<111> BCC
c     PROPS(49:56) - {112}<111>
c     PROPS(57:64) - {123}<111>
c
c     The Voce isotropic hardening law is implemented as
c     crss = tauC + tauSAT1*(1-exp(-GM/gmSAT1)) 
c                 + tauSAT2*(1-exp(-GM/gmSAT2))
c                 + tauSAT3*(1-exp(-GM/gmSAT3)) 
c                 + theta*GM
c
c ------------- PRINT PARAMETERS FIRST CALL OF UMAT -----------------
c 
      if (firstcall) then
c         print the material parameters in .dat file
          write(6,200) (PROPS(i),i=1,11)
          firstcall = .FALSE.
      end if
c
c
c ---------------------- INITIALIZE SLIP SYSTEMS ------------------------
c 
      if (int(PROPS(7)) .EQ. 1) full = .TRUE.
      call init_slipsystems(int(PROPS(6)), full, Nsg, Nslips, Nsmax,
     +                      nssgr, P, Omega)
c
c
c -------- INITIALIZATION OF SVDS ONLY FOR THE "ZERO" INCREMENT ---------
c 
      if (TIME(1) .LT. 1.d-16) then
c         this is executed for every IP only once at the beginning
c         initialize critical resolved shear stresses
		  j = 0
		  k = 0
          do i=1, Nslips
              if (i .GT. k) then
                  do
                      j = j + 1
                      if (Nsg(j) .NE. 0) exit
                  end do
                  k = k + Nsg(j)
              end if
              STATEV(25+i:) = PROPS(1+8*j)
          end do
c         initial Euler angles and the rotation matrix R
          ang = STATEV(1:3)
          call euler2rotm(ang, Q)
c         rotation matrix
          call mtransp(Q, 3, R)
c         store initial rotation matrix R
          STATEV(10:18) = reshape(R, [9])
      end if
c    
c
c ------------ CORRECT ROTATIONS OF "DSTRAN" AND "STRESS" ---------------
c     
c     1. if *ORIENTATION keyword is not used in Abaqus, i.e. no local sys.
c        is defined, then tensors DSTRAN, STRESS, DFGRD0, DFGRD1, DROT 
c        are given in the reference (global) system. In addition, STRESS 
c        is rotated by Abaqus as DROT * STRESS * DROT.T to take into 
c        account the rigid body rotations using Jaumann. Abaqus 
c        calculates DROT by the second-order scheme of Hughes and Winget 
c        as DROT = (I-0.5*DTIME*W)**(-1) * (I+0.5*DTIME*W). 
c        Here W = skw(L). It also applies, that DSTRAN = sym(L)*DTIME. 
c        L is here calculated as 
c        L = 0.5/DTIME *(DFGRD1-DFGRD0)*(inv(DFGRD1)+inv(DFGRD0))
c     2. Using the first order backward Euler numerical scheme, consitu- 
c        tive equations need to be solved in the corotational crystal 
c        system at time t(n+1). However, this is not known and instead of 
c        solving also the crystal orientation at time t(n+1) implicitly 
c        (which would increase the complexity of the implicit return map)
c        we choose to estimate the rotation of the crystal system at 
c        t(n+1) as Rest = DROT*Rn, i.e. assuming that crystal rotates by 
c        the Jaumann rate. 
c     3. Using this estimate, velocity gradient L is transformed to the 
c        crystal system as estimated by Rest at time t(n+1), as
c        Rest.T * L * Rest
c     4. As for the Cauchy stress, there is no need for the Jaumann  
c        contribution done automatically by Abaqus, this is "subtracted" 
c        as DROT.T * STRESS * DROT. Then the STRESS, which is from time 
c        t(n) is transformed into the crystal system at time t(n) as 
c        Rn.T * STRESS * Rn. 
c     5. After solving the crystal plasticity equations in the crystal 
c        sys., including plastic spin in the crystal system, W^hat_p, and
c        the constitutive spin W^hat_c = W^hat - W^hat_p, the rotation 
c        of the crystal is updated using equation Rdot = R*W^hat_c as 
c        R(n+1) = Rn*dR, where dR is calculated using Winget scheme.
c     6. The STRESS obtained from the return map shall be transformed   
c        back to the reference system by use of the updated crystal 
c        orientation given by the rotation R(n+1) as
c        R(n+1) * STRESS * R(n+1).T. 
c     7. The same R(n+1) is used also for unrotating the algorithmic 
c        modulus DDSDDE calculated in the crystal system at t(n+1).
c
c
c ---- VELOCITY GRADIENT L AT N+1/2 TIME STEP IN THE GLOBAL SYSTEM ------
c      
      oldF = DFGRD0
      newF = DFGRD1
c     second-order scheme to get L
      DF = (newF-oldF)*0.5d0/DTIME
      call minv3(newF)
      call minv3(oldF)
      call mmult(DF, newF+oldF, 3, Lg)
c
      Rn = reshape(STATEV(10:18), [3,3])
      call mmult(DROT, Rn, 3, Rest)
c
      S = reshape([STRESS(1), STRESS(4), STRESS(5),
     +             STRESS(4), STRESS(2), STRESS(6),
     +             STRESS(5), STRESS(6), STRESS(3)], [3,3])
c
      call transform(S, DROT, 3, Sg)
      call transform(Sg, Rn, 3, S)
c
      call transform(Lg, Rest, 3, L)
      call getsym(L, 3, D)
	  call getskw(L, 3, W)
c
      DEvec = [D(1,1), D(2,2), D(3,3), 
     +       2.d0*D(1,2), 2.d0*D(1,3), 2.d0*D(2,3)]*DTIME
      STRESS = [S(1,1), S(2,2), S(3,3), S(1,2), S(1,3), S(2,3)]
c
c ------- STRESS CONVERTION FROM ABAQUS-VOIGT TO NATURAL NOTATION -------
c 
      svec(1) = (STRESS(1)+STRESS(2)+STRESS(3))/sqrt(3.d0)
      svec(2) = (2.d0*STRESS(3)-STRESS(1)-STRESS(2))/sqrt(6.d0)
      svec(3) = (STRESS(2)-STRESS(1))/sqrt(2.d0)
      svec(4) = sqrt(2.d0)*STRESS(6)
      svec(5) = sqrt(2.d0)*STRESS(5)
      svec(6) = sqrt(2.d0)*STRESS(4)
c
      svec_old = svec
c
c ---- STRAIN INCR. CONVERTION FROM ABAQUS-VOIGT TO NATRUAL NOTATION ----
c 
      dstrain(1) = (DEvec(1)+DEvec(2)+DEvec(3))/sqrt(3.d0)
	  dstrain(2) = (2.d0*DEvec(3)-DEvec(1)-DEvec(2))/sqrt(6.d0)
	  dstrain(3) = (DEvec(2)-DEvec(1))/sqrt(2.d0)
	  dstrain(4) = DEvec(6)/sqrt(2.d0)
	  dstrain(5) = DEvec(5)/sqrt(2.d0)
	  dstrain(6) = DEvec(4)/sqrt(2.d0)
c
c ------------ THE CORE ROUTINE - RETURN-MAPPING ALGORITHM --------------
      call returnmap_hard1(Nslips, Nsg, svec, dstrain, DTIME, PROPS, 
     +                     NPROPS, STATEV, NSTATV, P, Nsmax, am, info)
c
c
c -------- UPDATE THE SPECIFIC ELASTIC AND PLASTIC STRAIN ENERGIES ------
c
c Elastic modulus for cubic symmetry (diagonal in the natural notation)
      Cdiag = [PROPS(2) + 2.d0*PROPS(3), 
     +         PROPS(2) - PROPS(3), 
     +         PROPS(2) - PROPS(3), 
     +         2.d0*PROPS(4), 
     +         2.d0*PROPS(4), 
     +         2.d0*PROPS(4)]
c     elastic energy
      DSSE = 0.d0
      do i=1, 6
          DSSE = DSSE + 1.d0/Cdiag(i)
     +        * (svec(i)+svec_old(i))*(svec(i)-svec_old(i))/2.d0
      end do
      SSE = SSE + DSSE
c     inc. of plastic E = inc. of total E - inc. of elastic E
      SPD = SPD + vdot(svec_old+svec, dstrain, 6)/2.d0 - DSSE
c
c
c ------ STRESS CONVERTION FROM NATURAL TO ABAQUS-VOIGT NOTATION --------
c   
      STRESS(1) = svec(1)/sqrt(3.d0) - svec(2)/sqrt(6.d0) 
     +          - svec(3)/sqrt(2.d0)
      STRESS(2) = svec(1)/sqrt(3.d0) - svec(2)/sqrt(6.d0) 
     +          + svec(3)/sqrt(2.d0)
      STRESS(3) = svec(1)/sqrt(3.d0) + svec(2)*2.d0/sqrt(6.d0)
      STRESS(4) = svec(6)/sqrt(2.d0)
      STRESS(5) = svec(5)/sqrt(2.d0)
      STRESS(6) = svec(4)/sqrt(2.d0)
c
c     save the Cauchy stress in the updated crystal system
      STATEV(20:25) = STRESS
c
c
c ------------------------ ORIENTATION UPDATE ---------------------------
c   
c     calculate constitutive spin as the total minus plastic
      wc = [W(2,3), W(1,3), W(1,2)]
      do j=1, Nslips
          wc = wc - Omega(:,j)*STATEV(25+Nslips+j)
      end do
	  W = 0.d0
      W(2,3) = wc(1)
      W(1,3) = wc(2)
      W(1,2) = wc(3)
      W(3,2) = -W(2,3)
      W(3,1) = -W(1,3)
      W(2,1) = -W(1,2)
c
      call mmult(W,W,3,DR)
      DR = DTIME/(1.d0+(DTIME/2.d0)**2
     +   * inner(W,W))*(W+(DTIME/2.d0)*DR)
      do i=1,3
	     DR(i,i) = DR(i,i) + 1.d0
	  end do
      call mmult(Rn, DR, 3, R)
c     store updated rotation of the crystal system R
      STATEV(10:18) = reshape(R, [9])
c     Euler angles
      call mtransp(R, 3, Q)
      call rotm2euler(Q, ang)
      STATEV(1:3) = ang
c
c
c ------------ ROTATE "STRESS" BACK TO THE REFERENCE SYSTEM -------------
c     
      S = reshape([STRESS(1), STRESS(4), STRESS(5), 
     +             STRESS(4), STRESS(2), STRESS(6),
     +             STRESS(5), STRESS(6), STRESS(3)], [3,3])
      call transformB(S, R, 3, Sg)
c     convert S into the Abaqus-Voigt vector notation
      STRESS = [Sg(1,1), Sg(2,2), Sg(3,3), Sg(1,2), Sg(1,3), Sg(2,3)]
c
c ------------ ROTATE ALG. MODULUS INTO THE REFERENCE SYSTEM ------------
c
      call get_rot5(R, R5)
	  call transformB(am(2:6,2:6), R5, 5, am0)
      am(2:6,2:6) = am0
c
c ---- ALG. MODULUS CONVERTION FROM NATURAL TO ABAQUS-VOIGT NOTATION ----
c
      DDSDDE(1,1) = (am(1,1) + 0.5d0*am(2,2) + sqrt(3.d0)*am(2,3)
     +            + 3.d0/2.d0*am(3,3))/3.d0
      DDSDDE(1,2) = (am(1,1) + 0.5d0*am(2,2) - 3.d0/2.d0*am(3,3))/3.d0
      DDSDDE(1,3) = (am(1,1) - am(2,2) - sqrt(3.d0)*am(2,3))/3.d0
      DDSDDE(1,4) = -0.5d0*(am(2,6)/sqrt(3.d0) + am(3,6))
      DDSDDE(1,5) = -0.5d0*(am(2,5)/sqrt(3.d0) + am(3,5))
      DDSDDE(1,6) = -0.5d0*(am(2,4)/sqrt(3.d0) + am(3,4))
      DDSDDE(2,2) = (am(1,1) + 0.5d0*am(2,2) - sqrt(3.d0)*am(2,3)
     +            + 3.d0/2.d0*am(3,3))/3.d0
      DDSDDE(2,3) = (am(1,1) - am(2,2) + sqrt(3.d0)*am(2,3))/3.d0
      DDSDDE(2,4) = -0.5d0*(am(2,6)/sqrt(3.d0) - am(3,6))
      DDSDDE(2,5) = -0.5d0*(am(2,5)/sqrt(3.d0) - am(3,5))
      DDSDDE(2,6) = -0.5d0*(am(2,4)/sqrt(3.d0) - am(3,4))
      DDSDDE(3,3) = (am(1,1) + 2.d0*am(2,2))/3.d0
      DDSDDE(3,4) = am(2,6)/sqrt(3.d0)
      DDSDDE(3,5) = am(2,5)/sqrt(3.d0)
      DDSDDE(3,6) = am(2,4)/sqrt(3.d0)
      DDSDDE(4,4) = 0.5d0*am(6,6)
      DDSDDE(4,5) = 0.5d0*am(5,6)
      DDSDDE(4,6) = 0.5d0*am(4,6)
      DDSDDE(5,5) = 0.5d0*am(5,5)
      DDSDDE(5,6) = 0.5d0*am(4,5)
      DDSDDE(6,6) = 0.5d0*am(4,4)
c
c     and symmetric lower triangle
      do i=1, 6
          do j=i+1, 6
              DDSDDE(j,i) = DDSDDE(i,j)
          end do
      end do
c
      return
c
c     http://patorjk.com/software/taag/#p=display&f=Big&t=UMAT
200   format(/
     +        5x, '***********************************'/,
     +        5x, '*   _    _ __  __       _______   *'/,
     +        5x, '*  | |  | |  \/  |   /\|__   __|  *'/,
     +        5x, '*  | |  | | \  / |  /  \  | |     *'/,
     +        5x, '*  | |  | | |\/| | / /\ \ | |     *'/,
     +        5x, '*  | |__| | |  | |/ ____ \| |     *'/,
     +        5x, '*   \____/|_|  |_/_/    \_\_|     *'/,
     +        5x, '*                                 *'/,
     +        5x, '***********************************'/,
     +        5x, '           Rate-independent        '/,
     +        5x, '     crystal plasticity based on   '/,
     +        5x, '     regularized single crystal    '/,
     +        5x, '            yield surface          '/,
     +        5x, '                                   '/,
     +        5x, '   Constitutive equations solved   '/,
     +        5x, '         by fully implicit         '/,
     +        5x, '       backward-Euler method       '/,
     +        5x, '                                   '/,
     +        5x, '            Implemented            '/,
     +        5x, '     in the natural notation       '/,
     +        5x, '                                   '/,
     +        5x, '              WITH                 '/,
     +        5x, '      MATERIAL PARAMETERS:         '/,
     +        5x, '                                   '/,
     +        5x, ' Elasticity                        '/,
     +        5x, ' C11      = ',               1pe12.5/,
     +        5x, ' C12      = ',               1pe12.5/,
     +        5x, ' C44      = ',               1pe12.5/,
     +        5x, ' Yield curface exponent            '/,
     +        5x, ' a        = ',               1pe12.5/,
     +        5x, ' Plasticity and hardening          '/, 
     +        5x, ' cs       = ',               1pe12.5/,
     +        5x, ' full     = ',               1pe12.5/,
     +        5x, ' tauCsat  = ',               1pe12.5/,
     +        5x, ' gmsat    = ',               1pe12.5/,
     +        5x, ' hself    = ',               1pe12.5/,
     +        5x, ' hlat     = ',               1pe12.5/,
     +        5x, ' relax    = ',               1pe12.5/)
      end subroutine umat
c
c
      subroutine returnmap_hard1(Nslips, Nsg, stress, dstrain, dt, 
     +                     params, np, sdv, ns, P, Nsmax, algmod, INFO)
c**********************************************************************
c      This is the return mapping predictor-corrector algorithm for 
c      solving the closest-point projection in an elastic-plastic 
c      problem for a regularized rate-independent crystal plasticity. 
c      It is based on a Newton-Raphson method with line-search.
c      This algorithm is described in details in
c      T. M�nik, H.M. Asadkandi, B. Holmedal. A robust algorithm 
c      for rate-independent crystal plasticity, Computer 
c      Methods in Applied Mechanics and Engineering, Volume 393. 
c      DOI https://doi.org/10.1016/j.cma.2022.114831"
c**********************************************************************
c (IN)     NSLIPS is INTEGER
c          It is the number of slip systems
c (IN)     NSG is INTEGER array, dimension (7)
c          An array with number of activated slip systems for each of
c          slip systems class
c (IN/OUT) STRESS is REAL*8 array, dimension (6)
c          It is Cauchy stress at the beginning of the time 
c          increment expressed in new notation. At return it is 
c          updated by the subroutine to be the stress at the end of the
c          time increment.
c (IN)     DSTRAIN is REAL*8 array, dimension (6)
c          It is the total strain increment expressed in new notation.
c (IN)     DT is REAL*8
c          It is a time increment
c (IN)     PARAMS is REAL*8 array, dimension (NP)
c          It contains user-defined necessary material parameters 
c          as e.g. elastic constants, plastic anisotropy coefficients...
c (IN)     NP is INTEGER
c          It defines the length of the PARAMS array.
c (IN)     SDV is REAL*8 array, dimension (NS)
c          It contains the solution-dependent state variables as e.g.
c          equivalent plastic strain, hardening model variables...
c (IN)     NS is INTEGER
c          It defines the length of the SDV array.
c (IN)     P is REAL*8 array, dimension (3,3,NSMAX)
c          It contains symmetric part of the Schmid matrix
c (IN)     NSMAX is INTEGER
c          Defines the maximum number of slip systems i.e. size
c          of the Schmid matrix
c (OUT)    ALGMOD is REAL*8 array, dimension (6,6)
c          It is the algorithmic modulus defined as dstress/dstrain.
c (OUT)    INFO is INTEGER
c          INFO = 1 means that the algorithm finished successfully
c          INFO = 2 means that the algorithm finished successfully 
c          within IMAX iterations but there was at least one 
c          iteration in which line-search was terminated due to 
c          reaching JMAX line-search iterations
c          INFO = 3 means that there was no need for plastic corrector
c          since the yield function at STRIAL <= 0.d0
c          INFO = -1 means unsuccesfull finish, i.e. the residual PSI
c          did not get below TOL within IMAX iterations
c
      implicit none  
c     ! in/out
      integer :: Nslips, Nsg, Nsmax, np, ns, jmax, imax, INFO
      real(8) :: stress(6), dstrain(6), dt, params(np), sdv(ns),
     +           algmod(6,6), P(5,Nsmax)
c     ! internal
      integer :: i, j, k, iter, jlargest, doprint,
     +           activesID(Nslips), Nact 
      real(8) :: Dp(5), DpI(5), res(5), strial(5), f, grad(5), q,
     +           hess(5,5), alpha, psi, lamdot,
     +           C11, C12, C44, a, GAMMA, phi, tau(Nslips), gm(Nslips),
     +           dlamdot, dsdev(5), lamdotI, sdevI(5), GAMMA0, GAMMAI,
     +           y(5), y2(5), sdev(5), denom, Cdiag(6), vdot, dGAMMA,
     +           crss(Nslips), psires, psif, psiq, 
     +           tolf, tolr, tolq, tolrx, tol_act, factor 
      logical :: full = .TRUE.
c
      jlargest = 0
c     return-map algorithm parameters
c     tolerance for the return-map algorithm
      tolf     = 1.d-8
      tolr     = 1.d-20
      tolq     = 1.d-16
      tolrx    = 1.d-6
c     tolerance for considering a slip system as active
      tol_act  = 0.d0
c     stress initial guess factor 
      factor   = 0.9d0
c     maximum number of Newton-Raphson iterations
      imax     = 999
c     maximum number of line-search iterations
      jmax     = 500
c
c --- PARAMETERS READING ------- 
c
c     Elastic constants for cubic symmetry
      C11 = params(2)
      C12 = params(3)
      C44 = params(4)
      Cdiag = [C11 + 2.d0*C12, C11 - C12, C11 - C12, 
     +         2.d0*C44, 2.d0*C44, 2.d0*C44]
c
c     exponent of the regularized yield function  
      a = params(5)
c     params(6) - slip system identificator
c     params(7) - differentiation of forward and reverse slips
      if (int(params(7)) .EQ. 0) full = .FALSE.
      doprint = int(params(8))
c
c --- SOLUTION-DEPENDENT VARIABLES READING ------- 
c
c     sdv(1:3) - Euler angles phi1, PHI, phi2
c     accumulated plastic strain
      GAMMA0 = sdv(4)
c     sdv(5)   - the plastic work rate
c     sdv(6)   - number of Newton-Raphson iterations to converge
c     sdv(7)   - the largest number of line-searches within one iter.
c     sdv(8)   - return-map convergence status (see INFO in returnmap)
c     sdv(9)   - the converged value of the residual PSI
c     sdv(10:18) - grain orientation matrix Q   
c     sdv(19)    - number of actives systems   
c     sdv(20:25) - co-rotated Cauchy stress
c     critical resolved shear stresses 
      crss = sdv(26:25+Nslips)
c     sdv(26+Nslips:25+2*Nslips) - slip rates   
c     
c
c --- ELASTIC PREDICTOR -------
c
c     hydrostatic pressure update
      stress(1) = stress(1) + Cdiag(1)*dstrain(1)
c
c     compute deviatoric trial stress
      strial = stress(2:) + Cdiag(2:)*dstrain(2:)
c
c     yield function at STRIAL
      call yldf(Nslips, full, strial, crss, P, Nsmax, a, 1, 
     +          phi, grad, hess, tau)
c
c     if no need for the plastic corrector
      if (phi .LT. 1.d0) then
c         update the stress
          stress(2:) = strial
c         caluclate the algorithmic modulus
          algmod = 0.d0
          do i=1, 6
              algmod(i,i) = Cdiag(i)
          end do
c         solution flag
          INFO = 3
c         update the state variables after pure elastic step
c         sdv(4) = no need to update
          sdv(5) = 0.d0
          sdv(6) = 0.d0
          sdv(7) = 0.d0
          sdv(8) = real(INFO)
          sdv(9) = 0.d0  
c         slip rates are zeros for pure elastic step													
          sdv(26+Nslips:25+2*Nslips) = 0.d0
c         number of active slips is zero										
		  sdv(19) = 0.d0
c         go to return
          goto 101
      end if
c
c --- PLASTIC CORRECTOR -------
c
c     initialization of stress, lamdot, and GAMMA
c     projection
c     note that for projection: grad(sdev) = grad(strial)
      sdev = strial/phi*factor
      tau  = tau/phi*factor
      phi  = factor
c     initialize plastic strain rate
      do i=1, 5
          Dp(i) = -(sdev(i) - strial(i))/Cdiag(i+1)/dt
      end do
c
c     initial guess for the plastic multiplier
      lamdot = vdot(Dp, sdev, 5)
c
c     improved initial guess for the accumulated total strain GAMMA
      call calc_slip(Nslips, tau, crss, phi, lamdot, a, 1, gm, 
     +               activesID, Nact)
c      GAMMA = GAMMA0 + sum(abs(gm))*dt
      GAMMA = GAMMA0
c
c     compute residuals for the improved guesses sdev, lamdot and GAMMA
c     note: f = 0.9 - 1 = -0.1
      res = -Dp + lamdot*grad
c     in general
      f   = phi - 1.d0
      q   = -(GAMMA-GAMMA0)/dt + sum(abs(gm))
c     calculate the merit function
      psires = vdot(res,res,5)*dt**2
      psif = f**2
      psiq = (q**2)*dt**2
      psi = 0.5d0*(psires + psif + psiq)
c
      if (doprint .EQ. 1) write(*, *) 'Psi initial ', psi
c
      if ((psires .LT. tolr) .AND. (psif   .LT. tolf) .AND.
     +    (psiq   .LT. tolq)) then 
          INFO = 0
          iter = 0
          goto 102
      end if
c
c     start of iterations
      do iter=1, imax
          call newton_step_hard1(Nslips, Nsg, full, strial, sdev, 
     +                    lamdot, GAMMA, GAMMA0, P, Nsmax, a, params, 
     +                    np, Cdiag, dt, dsdev, dlamdot, dGAMMA)

          call line_search_hard1(Nslips, Nsg, full, sdev, lamdot,  
     +                    GAMMA, dsdev, dlamdot, dGAMMA, strial,  
     +                    GAMMA0, P, Nsmax, a, params, np, Cdiag, dt, 
     +                    jmax, j, psi, psires, psif, psiq, alpha)
c       
          if (doprint .EQ. 1) write(*, 1000) iter, j, alpha, psi
1000      format(5x,'k =',I3,' j =',I3,' Alpha',f14.10,'  Psi',1pe16.8)
c
c         save max number of line-searches
          if (j .GT. jlargest) jlargest = j
c
          if (j .GE. jmax) INFO = 2
c         leave the loop if converged
          if ((psires .LT. tolr) .AND. (psif   .LT. tolf) .AND.
     +        (psiq   .LT. tolq)) then 
              INFO = 1
              goto 102
          end if
      end do
c
      INFO = -1
c
c --- STRESS UPDATE (DEVIATORIC PART) -------
c
102   continue
      do i=1,5
          stress(i+1) = sdev(i)
      end do
c
c --- ALGORITHMIC MODULUS UPDATE -------
c
      call algmod_hard1(Nslips, Nsg, full, strial, sdev, lamdot, GAMMA, 
     +                GAMMA0, P, Nsmax, a, params, np, Cdiag, dt, crss,
     +                gm, Nact, algmod) 
c
c --- SOLUTION-DEPENDENT VARIABLES UPDATE -------
c
c     accumulated plastic strain
      sdv(4) = GAMMA
c     plastic work rate
      sdv(5) = lamdot
c     number of iterations used
      sdv(6) = real(iter)
c     max number of line-searches for one Newton iter
      sdv(7) = real(jlargest)
c     convergence INFO
      sdv(8) = real(INFO)
c     final value of PSI residual
      sdv(9) = psi
c     critical resolved shear stresses   
      sdv(26:25+Nslips) = crss
c     slip rates
      sdv(26+Nslips:25+2*Nslips) = gm
c     number of actives systems
      sdv(19) = real(Nact)
c     rest of svd's is updated in umat
c
101   return
      end subroutine returnmap_hard1
c
c
      subroutine algmod_hard1x(Nslips, Nsg, full, strial, sdev, lamdot, 
     +                        GAMMA, GAMMA0, P, Nsmax, a, params, np, 
     +                        Cdiag, dt, crss, gm, Nact, algmod)
c
      implicit none
c     input
      integer :: Nslips, np, Nsmax, Nsg(7)
      real(8) :: strial(5), GAMMA, GAMMA0, P(5,Nsmax), a, Cdiag(6), dt,
     +           sdev(5), lamdot, params(np)
      logical :: full
c     output
      integer :: Nact
      real(8) :: crss(Nslips), gm(Nslips), algmod(6,6)
c
      integer :: i, j, activesID(Nslips)
      real(8) :: phi, grad(5), hess(5,5), f, Dp(5), Linv(5,5), L(5,5), 
     +           h(Nslips), choldiag(5), tau(Nslips), dGMDOTdlamdot,
     +           dfdGM, ddfdSdGM(5), dGMDOTdS(5), dGMDOTdGM, vdot, Y,
     +           GMDOT, Lgrad(5), tmp, tmp0, ySGM(5),
     +           tmp1(5), tmp2(5,5), tmp3(5), tmp4(5), tmp5(5), 
     +           tmp6(5), tmp7(5,5)
c
c     calculate plastic strain rate
      do i=1, 5
          Dp(i) = -(sdev(i)-strial(i))/Cdiag(i+1)/dt
      end do
c     calculate critical resolved shear stresses and the hardening rates
      call calc_h1(Nslips, Nsg, params, np, GAMMA, GAMMA0, 1, crss, h)
c     calculate yield func, its gradient and hessian
      call yldf(Nslips, full, sdev, crss, P, Nsmax, a, 2, phi, grad, 
     +          hess, tau) 
c     compute slip rates
      call calc_slip(Nslips, tau, crss, phi, lamdot, a, 1, gm, 
     +               activesID, Nact)
c     calculate GAMMADOT
      GMDOT = sum(abs(gm))
c     compute the necessary derivatives
      call calc_deriv_hard1(Nslips, Nsg, full, phi, grad, P, Nsmax, 
     +                 crss, tau, lamdot, h, GMDOT, a, dfdGM, ddfdSdGM, 
     +                 dGMDOTdS, dGMDOTdlamdot, dGMDOTdGM)
c
c
c     compute inverse of L matrix
      Linv = lamdot*hess
      do i=1, 5
          Linv(i,i) = Linv(i,i) + 1.d0/Cdiag(i+1)/dt
      end do 
c     compute L
      call chol_decomp(Linv, 5, choldiag)
      call chol_inverse(Linv, 5, choldiag, L)
c     calculate Y (scalar)
      call mvmult(L, ddfdSdGM, 5, ySGM)
      Y = 1.d0/(dGMDOTdGM - 1.d0/dt
     +  - lamdot*vdot(dGMDOTdS,ySGM,5))
c
      tmp = Y*(lamdot*vdot(grad,ySGM,5)-dfdGM)
	  call mvmult(L, dGMDOTdS, 5, tmp1)
	  tmp4 = tmp*tmp1
	  call mvmult(L,grad,5,Lgrad)
	  tmp3 = Lgrad + tmp4
      tmp0 = vdot(dGMDOTdS,Lgrad,5)-dGMDOTdlamdot
      tmp3 = tmp3/(vdot(grad,Lgrad,5)+tmp*tmp0)
c
      tmp1 = -Y*tmp1 + Y*tmp0*tmp3
c
      call outer(ddfdSdGM,tmp1,5,tmp2)
	  tmp2 = lamdot*tmp2
	  call outer(grad,tmp3,5,tmp7)
	  tmp7 = -tmp7 - tmp2
      do i=1, 5
          tmp7(i,i) = tmp7(i,i) + 1.d0
      end do
      call mmult(L, tmp7, 5, tmp2)
c
      algmod(1,1) = Cdiag(1)
      algmod(1,2:) = 0.d0
      algmod(2:,1) = 0.d0
      algmod(2:,2:) = tmp2/dt
      return
      end subroutine algmod_hard1x
c
c
      subroutine algmod_hard1(Nslips, Nsg, full, strial, sdev, lamdot, 
     +                        GAMMA, GAMMA0, P, Nsmax, a, params, np, 
     +                        Cdiag, dt, crss, gm, Nact, algmod)
c
      implicit none
c     input
      integer :: Nslips, np, Nsmax, Nsg(7)
      real(8) :: strial(5), GAMMA, GAMMA0, P(5,Nsmax), a, Cdiag(6), dt,
     +           sdev(5), lamdot, params(np)
      logical :: full
c     output
      integer :: Nact
      real(8) :: crss(Nslips), gm(Nslips), algmod(6,6)
c
      integer :: i, j, activesID(Nslips)
      real(8) :: phi, grad(5), hess(5,5), f, Dp(5), Linv(5,5), L(5,5), 
     +           h(Nslips), choldiag(5), tau(Nslips), dGMDOTdlamdot,
     +           dfdGM, ddfdSdGM(5), dGMDOTdS(5), dGMDOTdGM, vdot, Y,
     +           GMDOT, M(5,5), MT(5,5), denom,
     +           tmp1(5), tmp2(5,5), tmp3(5), tmp4(5), tmp5(5), tmp6(5)
c
c     calculate plastic strain rate
      do i=1, 5
          Dp(i) = -(sdev(i)-strial(i))/Cdiag(i+1)/dt
      end do
c     calculate critical resolved shear stresses and the hardening rates
      call calc_h1(Nslips, Nsg, params, np, GAMMA, GAMMA0, 1, crss, h)
c     calculate yield func, its gradient and hessian
      call yldf(Nslips, full, sdev, crss, P, Nsmax, a, 2, phi, grad, 
     +          hess, tau) 
c     compute slip rates
      call calc_slip(Nslips, tau, crss, phi, lamdot, a, 1, gm, 
     +               activesID, Nact)
c     calculate GAMMADOT
      GMDOT = sum(abs(gm))
c     compute the necessary derivatives
      call calc_deriv_hard1(Nslips, Nsg, full, phi, grad, P, Nsmax, 
     +                 crss, tau, lamdot, h, GMDOT, a, dfdGM, ddfdSdGM, 
     +                 dGMDOTdS, dGMDOTdlamdot, dGMDOTdGM)
c
c
c     compute inverse of L matrix
      Linv = lamdot*hess
      do i=1, 5
          Linv(i,i) = Linv(i,i) + 1.d0/Cdiag(i+1)/dt
      end do 
c     compute L
      call chol_decomp(Linv, 5, choldiag)
      call chol_inverse(Linv, 5, choldiag, L)
c     calculate Y (scalar)
      call mvmult(L, ddfdSdGM, 5, tmp1)
      Y = 1.d0/(dGMDOTdGM - 1.d0/dt
     +  - lamdot*vdot(dGMDOTdS,tmp1,5))
c     compute M
      call outer(tmp1, dGMDOTdS, 5, tmp2)
      tmp2 = lamdot*Y*tmp2
      do i=1, 5
          tmp2(i,i) = tmp2(i,i) + 1.d0
      end do
      call mmult(tmp2, L, 5, M)
c       
c
      tmp3 = Y*lamdot*dGMDOTdlamdot*tmp1
      call mvmult(M, grad, 5, tmp4)
      call mvmult(L, dGMDOTdS, 5, tmp5)
      tmp5 = dfdGM*Y*tmp5
      call mtransp(M, 5, MT)
      call mvmult(MT, grad, 5, tmp6)
      tmp6 = tmp5 - tmp6
      call outer(tmp4-tmp3, tmp6, 5, tmp2)
      denom = -vdot(grad,tmp4-tmp3,5)+vdot(tmp5,grad,5)
     +        -dfdGM*Y*dGMDOTdlamdot
      algmod(1,1) = Cdiag(1)
      do i=2,6
          algmod(1,i) = 0.d0
          algmod(i,1) = 0.d0
      end do
      do i=2,6
          do j=2,6
              algmod(i,j) = (M(i-1,j-1) - tmp2(i-1,j-1)/denom)/dt
          end do
      end do
c     
      return
      end subroutine algmod_hard1
c
c
      subroutine newton_step_hard1(Nslips, Nsg, full, strial, sdev, 
     +                      lamdot, GAMMA, GAMMA0, P, Nsmax, a, params,
     +                      np,Cdiag, dt, dsdev, dlamdot, dGAMMA) 
c
      implicit none
c     input
      integer :: Nslips, np, Nsg(7), Nsmax
      real(8) :: strial(5), GAMMA, GAMMA0, P(5,Nsmax), a, Cdiag(6), dt,
     +           sdev(5), lamdot, params(np)
      logical :: full
c     output
      real(8) :: dsdev(5), dlamdot, dGAMMA
c
      integer :: i, IER, IPL(5), activesID(Nslips), Nact
      real(8) :: phi, grad(5), hess(5,5), f, Dp(5), 
     +           res(5), h(Nslips), tau(Nslips), gm(Nslips), q, Y,
     +           dfdGM, ddfdSdGM(5), dGMDOTdS(5), dGMDOTdGM, 
     +           Linv(5,5), dGMDOTdlamdot, yr(5), ygrad(5), ySGM(5), 
     +           crss(Nslips), GMDOT, vdot, tmp1, tmp2, tmp3
c
c     calculate plastic strain rate
      do i=1, 5
          Dp(i) = -(sdev(i)-strial(i))/Cdiag(i+1)/dt
      end do
c     calculate critical resolved shear stresses and the hardening rates
      call calc_h1(Nslips, Nsg, params, np, GAMMA, GAMMA0, 1, crss, h)
c     calculate yield func, its gradient and hessian
      call yldf(Nslips, full, sdev, crss, P, Nsmax, a, 2, 
     +          phi, grad, hess, tau) 
c     compute slip rates
      call calc_slip(Nslips, tau, crss, phi, lamdot, a, 1, gm, 
     +               activesID, Nact)
c     calculate GAMMADOT
      GMDOT = sum(abs(gm))
c     copmute the necessary derivatives
      call calc_deriv_hard1(Nslips, Nsg, full, phi, grad, P, Nsmax, 
     +                    crss, tau, lamdot, h, GMDOT, a, dfdGM,  
     +                    ddfdSdGM, dGMDOTdS, dGMDOTdlamdot, dGMDOTdGM)
c                
c     calculate residuals
      res = -Dp + lamdot*grad
      f   = phi - 1.d0
      q   = -(GAMMA-GAMMA0)/dt + GMDOT
c
c     compute inverse of L matrix
      Linv = lamdot*hess
      do i=1, 5
          Linv(i,i) = Linv(i,i) + 1.d0/Cdiag(i+1)/dt
      end do 
      call DEC(5, 5, Linv, IPL, IER)
c
      yr = res
      call SOL(5, 5, Linv, yr, IPL)
      ygrad = grad
      call SOL(5, 5, Linv, ygrad, IPL)
      ySGM = ddfdSdGM
      call SOL(5, 5, Linv, ySGM, IPL)
c       
c     calculate Y (scalar)
      Y = 1.d0/(dGMDOTdGM-1.d0/dt-lamdot*vdot(dGMDOTdS,ySGM,5))
c
      tmp1 = Y*(lamdot*vdot(grad,ySGM,5)-dfdGM)
      tmp2 = vdot(dGMDOTdS,yr,5)-q
      tmp3 = vdot(dGMDOTdS,ygrad,5)-dGMDOTdlamdot
      dlamdot = f - vdot(grad,yr,5) - tmp1*tmp2
      dlamdot = dlamdot/(vdot(grad,ygrad,5)+tmp1*tmp3)
c
      dGAMMA = Y*tmp2 + Y*tmp3*dlamdot
      dsdev = -yr - ygrad*dlamdot - lamdot*ySGM*dGAMMA
c     
      return
      end subroutine newton_step_hard1
c
c
      subroutine line_search_hard1(Nslips, Nsg, full, sdev, lamdot,  
     +                      GAMMA, dsdev, dlamdot, dGAMMA, strial, 
     +                      GAMMA0, P, Nsmax, a, params, np, Cdiag, dt, 
     +                      jmax, j, psi, psires, psif, psiq, alph)
c
      implicit none 
c     input
      integer :: Nslips, Nsg(7), jmax, np, Nsmax
      real(8) :: strial(5), GAMMA0, P(5,Nsmax), a, Cdiag(6), dt, 
     +           params(np), dsdev(5), dlamdot, dGAMMA
      logical :: full
c     input/output
      real(8) :: sdev(5), lamdot, GAMMA
c     output
      integer :: j
      real(8) :: psi, psires, psif, psiq, alph
c
      integer :: i, k, activesID(Nslips), Nact, status
      real(8) :: phi, grad(5), hess(5,5), f, Dp(5), q, DpI(5),
     +           res(5), h(Nslips), tau(Nslips), gm(Nslips),
     +           sdevI(5), lamdotI, eps, eps0, alphL, alphR, GAMMAI,
     +           crssI(Nslips), psi0, wa(15)
c
      wa = 0.d0
      eps = min(0.3d0, 5.d0/a)
      eps0 = 1.d-2*eps
      status = 0
      alphL = 0.d0
      alphR = 1.d0
      alph  = alphL
c        

      sdevI   = sdev
      lamdotI = lamdot
      GAMMAI  = GAMMA
      do i=1, 5
          DpI(i) = -(sdevI(i) - strial(i))/Cdiag(i+1)/dt
      end do  
c     calculate critical resolved shear stresses
      call calc_h1(Nslips, Nsg, params, np, GAMMAI, GAMMA0, 0, crssI, h)
c
      call meritfun_hard1(Nslips, full, sdevI, DpI, lamdotI, crssI, 
     +         GAMMAI, dt, GAMMA0, P, Nsmax, a, psi0, psires, psif, psiq)
c
      do j=1, jmax
          call minalg(alph, psi, alphL, alphR, psi0, eps, eps0, 
     +                status, wa)
          if (status .EQ. 0) goto 100
c
          sdevI = sdev + alph*dsdev
          lamdotI = max(0.d0, lamdot + alph*dlamdot)
          GAMMAI = max(0.d0, GAMMA + alph*dGAMMA)
c         calculate plastic strain rate
          do i=1, 5
              DpI(i) = -(sdevI(i) - strial(i))/Cdiag(i+1)/dt
          end do  
c         calculate critical resolved shear stresses
          call calc_h1(Nslips, Nsg, params, np, GAMMAI, GAMMA0, 0, 
     +                 crssI, h)
          call meritfun_hard1(Nslips, full, sdevI, DpI, lamdotI, crssI, 
     +          GAMMAI, dt, GAMMA0, P, Nsmax, a, psi, psires, psif, psiq)
      end do    
c        
100   continue    
c     update of the unknowns
      sdev = sdevI
      lamdot = lamdotI
      GAMMA = GAMMAI
c     
      return
      end subroutine line_search_hard1
c
c
      subroutine calc_deriv_hard1(Nslips, Nsg, full, phi, grad, P, Nsmax, 
     +                      crss, tau, lamdot, h, GMDOT, a, dfdGM, 
     +                      ddfdSdGM, dGMDOTdS, dGMDOTdlamdot, dGMDOTdGM)
c
      implicit none
c    
c     input
      integer :: Nslips, Nsg(7), Nsmax
      real(8) :: crss(Nslips), lamdot, a, P(5,Nsmax), phi, grad(5), 
     +           h(Nslips), tau(Nslips), GMDOT
      logical :: full
c     output
      real(8) :: dfdGM, ddfdSdGM(5), dGMDOTdS(5), dGMDOTdlamdot, 
     +           dGMDOTdGM
c
      integer :: i
c    
      intent(in)  :: Nslips, Nsg, full, phi, grad, P, Nsmax, crss, tau,  
     +               lamdot, h, a
      intent(out) :: dfdGM, ddfdSdGM, dGMDOTdS, dGMDOTdlamdot, dGMDOTdGM
c      
c     compute dfdGM
      dfdGM = 0.d0
      do i=1, Nslips
          dfdGM = dfdGM - phi*h(i)/crss(i)
     +          * (abs(tau(i))/(phi*crss(i)))**a
      end do
c
c     compute ddfdSdGM
      ddfdSdGM = 0.d0
      do i=1, Nslips
          ddfdSdGM = ddfdSdGM
     +    - a*sign(1.d0,tau(i))*h(i)*P(:,i)/(crss(i)**2)
     +    * (abs(tau(i))/(phi*crss(i)))**(a-1.d0)
      end do
      ddfdSdGM = ddfdSdGM + (1.d0-a)*dfdGM/phi*grad

c     compute dGMDOTdS
      dGMDOTdS = 0.d0
      do i=1, Nslips
          dGMDOTdS = dGMDOTdS + (a-1.d0)/phi*abs(lamdot)
     +             / (crss(i)**2) * sign(1.d0,tau(i))*P(:,i)
     +             * (abs(tau(i))/(phi*crss(i)))**(a-2.d0)
      end do
      dGMDOTdS = dGMDOTdS - (a-1.d0)*GMDOT*grad/phi
c
c     compute dGMDOTdlamdot
      dGMDOTdlamdot = 0.d0
      do i=1, Nslips
          dGMDOTdlamdot = dGMDOTdlamdot + 1.d0/crss(i)
     +                  * (abs(tau(i))/(phi*crss(i)))**(a-1.d0)
      end do
      dGMDOTdlamdot = dGMDOTdlamdot*sign(1.d0,lamdot)
c 
c     compute dGMDOTdGM
      dGMDOTdGM = 0.d0
      do i=1, Nslips
          dGMDOTdGM = dGMDOTdGM - abs(lamdot)*a*h(i)/(crss(i)**2)
     +              * (abs(tau(i))/(phi*crss(i)))**(a-1.d0)
      end do
      dGMDOTdGM = dGMDOTdGM + (1.d0-a)*GMDOT*dfdGM/phi
c
      return
      end subroutine calc_deriv_hard1
c
c
      subroutine meritfun_hard1(Nslips, full, sdev, Dp, lamdot, crss, 
     +                          GAMMA, dt, GAMMA0, P, Nsmax, a,  
     +                          psi, psires, psif, psiq)
c
      implicit none
c     input
      integer :: Nslips, Nsmax
      real(8) :: sdev(5), dtauc(Nslips), crss(Nslips), GAMMA, GAMMA0,
     +           a, Dp(5), lamdot, dt, P(5, Nsmax)
      logical :: full
c     output
      real(8) :: psi, psires, psif, psiq
c     
      integer :: activesID(Nslips), Nact
      real(8) :: f, phi, grad(5), hess(5,5), gm(Nslips), tau(Nslips), 
     +           h(Nslips), q, res(5), vdot
c     
      intent(in)  :: Nslips, full, sdev, Dp, lamdot, GAMMA, GAMMA0, dt,
     +               crss, a, P, Nsmax
      intent(out) :: psi, psires, psif, psiq
c     
      call yldf(Nslips, full, sdev, crss, P, Nsmax, a, 1, 
     +          phi, grad, hess, tau)
      call calc_slip(Nslips, tau, crss, phi, lamdot, a, 1, gm, 
     +               activesID, Nact)
c 
      res = -Dp + lamdot*grad
      f   = phi - 1.d0
      q   = -(GAMMA-GAMMA0)/dt + sum(abs(gm))
c     calculate the merit function
      psires = vdot(res,res,5)*dt**2
      psif = f**2
      psiq = (q**2)*dt**2
      psi = 0.5d0*(psires + psif + psiq)
c
      end subroutine meritfun_hard1
c
c
      subroutine init_slipsystems(cs, full, Nsg, Nslips, Nsmax, 
     +                            nssgr, P, Omega)
c     Subroutine defines slip systems, build Schmid matrix, its 
c     symmetric part "P" and skew part "Omega"
c    
c     Input: sizes - specifies the set of slip systems used for the 
c                    calculations as:
c
c          FCC
c     '(111) <1-10>'
c     '(110) <1-10>' 
c     '(100) <1-10>'
c     '(112) <1-10>'
c         
c          BCC
c     '(101) <11-1>'
c     '(112) <11-1>'
c     '(123) <11-1>'
c
      implicit none
c  
      real(8), parameter  :: inv3  = 1.d0/sqrt(3.d0),
     +                       inv2  = 1.d0/sqrt(2.d0),
     +                       inv6  = 1.d0/sqrt(6.d0),
     +                       inv14 = 1.d0/sqrt(14.d0)
c
c     input
      integer :: cs, Nsmax, nssgr
      logical :: full
c     output
      integer :: Nslips, Nsg(nssgr)
      real(8) :: P(5,Nsmax), Omega(3,Nsmax)
c     
      integer :: i, s, r, k, Nsizes, csvec(nssgr), Nsg_temp(nssgr),
     +           sizes(nssgr)
      real(8) :: n(Nsmax,3), b(Nsmax,3), schmid(3,3,Nsmax), 
     +           schmid_sym(3,3,Nsmax), schmid_skw_vec(3,Nsmax),
     +           schmid_skw(3,3,Nsmax), schmid_sym_vec(5,Nsmax), tmp(6)
c  
c     number of slip systems in each of the slip classes if both positive
c     and negative directions are counted  
      sizes = [24, 12, 12, 24, 24, 24, 48]
      Nsizes = sum(sizes)
      do i=1, nssgr 
          csvec(nssgr+1-i) = mod(cs, 10)
          cs = (cs - mod(cs, 10))/10
      end do
c     number of slip systems in each slip class required
      Nsg_temp = csvec*sizes
c     slip systems described by slip plane normal "n" and slip direction "b"
c     slip plane normals "n"
c     cs = 1000000 (FCC octahedral slips)
      n(1,:)  = [ 1.d0,  1.d0, -1.d0]
      n(2,:)  = [ 1.d0,  1.d0, -1.d0]
      n(3,:)  = [ 1.d0,  1.d0, -1.d0]
      n(4,:)  = [ 1.d0, -1.d0, -1.d0]
      n(5,:)  = [ 1.d0, -1.d0, -1.d0]
      n(6,:)  = [ 1.d0, -1.d0, -1.d0]
      n(7,:)  = [ 1.d0, -1.d0,  1.d0]
      n(8,:)  = [ 1.d0, -1.d0,  1.d0]
      n(9,:)  = [ 1.d0, -1.d0,  1.d0]
      n(10,:) = [ 1.d0,  1.d0,  1.d0]
      n(11,:) = [ 1.d0,  1.d0,  1.d0]
      n(12,:) = [ 1.d0,  1.d0,  1.d0]
c     cs = 0100000
      n(13,:) = [ 1.d0,  1.d0,  0.d0]
      n(14,:) = [ 1.d0, -1.d0,  0.d0]
      n(15,:) = [ 1.d0,  0.d0,  1.d0]
      n(16,:) = [ 1.d0,  0.d0, -1.d0]
      n(17,:) = [ 0.d0,  1.d0,  1.d0]
      n(18,:) = [ 0.d0,  1.d0, -1.d0]
c     cs = 0010000
      n(19,:) = [ 1.d0,  0.d0,  0.d0]
      n(20,:) = [ 1.d0,  0.d0,  0.d0]
      n(21,:) = [ 0.d0,  1.d0,  0.d0]
      n(22,:) = [ 0.d0,  1.d0,  0.d0]
      n(23,:) = [ 0.d0,  0.d0,  1.d0]
      n(24,:) = [ 0.d0,  0.d0,  1.d0]
c     cs = 0001000
      n(25,:) = [-1.d0,  1.d0,  2.d0]
      n(26,:) = [-1.d0,  1.d0, -2.d0]
      n(27,:) = [ 1.d0,  1.d0,  2.d0]
      n(28,:) = [ 1.d0,  1.d0, -2.d0]
      n(29,:) = [ 1.d0,  2.d0, -1.d0]
      n(30,:) = [ 1.d0, -2.d0, -1.d0]
      n(31,:) = [ 1.d0,  2.d0,  1.d0]
      n(32,:) = [ 1.d0, -2.d0,  1.d0]
      n(33,:) = [ 2.d0,  1.d0, -1.d0]
      n(34,:) = [-2.d0,  1.d0, -1.d0]
      n(35,:) = [ 2.d0,  1.d0,  1.d0]
      n(36,:) = [-2.d0,  1.d0,  1.d0]
c     cs = 0000100 (BCC base)
      n(37,:) = [ 1.d0,  1.d0,  0.d0]
      n(38,:) = [ 1.d0,  1.d0,  0.d0]
      n(39,:) = [ 1.d0, -1.d0,  0.d0]
      n(40,:) = [ 1.d0, -1.d0,  0.d0]
      n(41,:) = [ 0.d0,  1.d0,  1.d0]
      n(42,:) = [ 0.d0,  1.d0,  1.d0]
      n(43,:) = [ 0.d0, -1.d0,  1.d0]
      n(44,:) = [ 0.d0, -1.d0,  1.d0]
      n(45,:) = [ 1.d0,  0.d0,  1.d0]
      n(46,:) = [ 1.d0,  0.d0,  1.d0]
      n(47,:) = [ 1.d0,  0.d0, -1.d0]
      n(48,:) = [ 1.d0,  0.d0, -1.d0]
c     cs = 0000010 (BCC base)
      n(49,:) = [-1.d0,  1.d0,  2.d0]
      n(50,:) = [-1.d0,  1.d0, -2.d0]
      n(51,:) = [ 1.d0,  1.d0,  2.d0]
      n(52,:) = [ 1.d0,  1.d0, -2.d0]
      n(53,:) = [ 1.d0,  2.d0, -1.d0]
      n(54,:) = [ 1.d0, -2.d0, -1.d0]
      n(55,:) = [ 1.d0,  2.d0,  1.d0]
      n(56,:) = [ 1.d0, -2.d0,  1.d0]
      n(57,:) = [ 2.d0,  1.d0, -1.d0]
      n(58,:) = [-2.d0,  1.d0, -1.d0]
      n(59,:) = [ 2.d0,  1.d0,  1.d0]
      n(60,:) = [-2.d0,  1.d0,  1.d0]
c     cs = 0000001 (BCC)
      n(61,:) = [ 1.d0,  2.d0,  3.d0]
      n(62,:) = [-1.d0,  2.d0,  3.d0]
      n(63,:) = [ 1.d0, -2.d0,  3.d0]
      n(64,:) = [ 1.d0,  2.d0, -3.d0]
      n(65,:) = [ 1.d0,  3.d0,  2.d0]
      n(66,:) = [-1.d0,  3.d0,  2.d0]
      n(67,:) = [ 1.d0, -3.d0,  2.d0]
      n(68,:) = [ 1.d0,  3.d0, -2.d0]
      n(69,:) = [ 2.d0,  1.d0,  3.d0]
      n(70,:) = [-2.d0,  1.d0,  3.d0]
      n(71,:) = [ 2.d0, -1.d0,  3.d0]
      n(72,:) = [ 2.d0,  1.d0, -3.d0]
      n(73,:) = [ 2.d0,  3.d0,  1.d0]
      n(74,:) = [-2.d0,  3.d0,  1.d0]
      n(75,:) = [ 2.d0, -3.d0,  1.d0]
      n(76,:) = [ 2.d0,  3.d0, -1.d0]
      n(77,:) = [ 3.d0,  1.d0,  2.d0]
      n(78,:) = [-3.d0,  1.d0,  2.d0]
      n(79,:) = [ 3.d0, -1.d0,  2.d0]
      n(80,:) = [ 3.d0,  1.d0, -2.d0]
      n(81,:) = [ 3.d0,  2.d0,  1.d0]
      n(82,:) = [-3.d0,  2.d0,  1.d0]
      n(83,:) = [ 3.d0, -2.d0,  1.d0]
      n(84,:) = [ 3.d0,  2.d0, -1.d0]
c
c     slip directions "b"
c     cs = 1000000
      b(1,:)  = [ 0.d0,  1.d0,  1.d0]
      b(2,:)  = [ 1.d0,  0.d0,  1.d0]
      b(3,:)  = [ 1.d0, -1.d0,  0.d0]
      b(4,:)  = [ 0.d0,  1.d0, -1.d0]
      b(5,:)  = [ 1.d0,  0.d0,  1.d0]
      b(6,:)  = [ 1.d0,  1.d0,  0.d0]
      b(7,:)  = [ 0.d0,  1.d0,  1.d0]
      b(8,:)  = [ 1.d0,  0.d0, -1.d0]
      b(9,:)  = [ 1.d0,  1.d0,  0.d0]
      b(10,:) = [ 0.d0,  1.d0, -1.d0]
      b(11,:) = [ 1.d0,  0.d0, -1.d0]
      b(12,:) = [ 1.d0, -1.d0,  0.d0]
c     cs = 0100000
      b(13,:) = [ 1.d0, -1.d0,  0.d0]
      b(14,:) = [ 1.d0,  1.d0,  0.d0]
      b(15,:) = [ 1.d0,  0.d0, -1.d0]
      b(16,:) = [ 1.d0,  0.d0,  1.d0]
      b(17,:) = [ 0.d0,  1.d0, -1.d0]
      b(18,:) = [ 0.d0,  1.d0,  1.d0]
c     cs = 0010000
      b(19,:) = [ 0.d0,  1.d0,  1.d0]
      b(20,:) = [ 0.d0,  1.d0, -1.d0]
      b(21,:) = [ 1.d0,  0.d0,  1.d0]
      b(22,:) = [ 1.d0,  0.d0, -1.d0]
      b(23,:) = [ 1.d0,  1.d0,  0.d0]
      b(24,:) = [ 1.d0, -1.d0,  0.d0]
c     cs = 0001000
      b(25,:) = [ 1.d0,  1.d0,  0.d0]
      b(26,:) = [ 1.d0,  1.d0,  0.d0]
      b(27,:) = [ 1.d0, -1.d0,  0.d0]
      b(28,:) = [ 1.d0, -1.d0,  0.d0]
      b(29,:) = [ 1.d0,  0.d0,  1.d0]
      b(30,:) = [ 1.d0,  0.d0,  1.d0]
      b(31,:) = [ 1.d0,  0.d0, -1.d0]
      b(32,:) = [ 1.d0,  0.d0, -1.d0]
      b(33,:) = [ 0.d0,  1.d0,  1.d0]
      b(34,:) = [ 0.d0,  1.d0,  1.d0]
      b(35,:) = [ 0.d0,  1.d0, -1.d0]
      b(36,:) = [ 0.d0,  1.d0, -1.d0]
c     cs = 0000100 (BCC)
      b(37,:) = [ 1.d0, -1.d0,  1.d0]
      b(38,:) = [ 1.d0, -1.d0, -1.d0]
      b(39,:) = [ 1.d0,  1.d0,  1.d0]
      b(40,:) = [ 1.d0,  1.d0, -1.d0]
      b(41,:) = [ 1.d0, -1.d0,  1.d0]
      b(42,:) = [-1.d0, -1.d0,  1.d0]
      b(43,:) = [ 1.d0,  1.d0,  1.d0]
      b(44,:) = [-1.d0,  1.d0,  1.d0]
      b(45,:) = [ 1.d0,  1.d0, -1.d0]
      b(46,:) = [ 1.d0, -1.d0, -1.d0]
      b(47,:) = [ 1.d0,  1.d0,  1.d0]
      b(48,:) = [ 1.d0, -1.d0,  1.d0]
c     cs = 0000010 (BCC)
      b(49,:) = [ 1.d0, -1.d0,  1.d0]
      b(50,:) = [-1.d0,  1.d0,  1.d0]
      b(51,:) = [ 1.d0,  1.d0, -1.d0]
      b(52,:) = [ 1.d0,  1.d0,  1.d0]
      b(53,:) = [-1.d0,  1.d0,  1.d0]
      b(54,:) = [ 1.d0,  1.d0, -1.d0]
      b(55,:) = [ 1.d0, -1.d0,  1.d0]
      b(56,:) = [ 1.d0,  1.d0,  1.d0]
      b(57,:) = [ 1.d0, -1.d0,  1.d0]
      b(58,:) = [ 1.d0,  1.d0, -1.d0]
      b(59,:) = [-1.d0,  1.d0,  1.d0]
      b(60,:) = [ 1.d0,  1.d0,  1.d0]
c     cs = 0000001 (BCC)
      b(61,:) = [ 1.d0,  1.d0, -1.d0]
      b(62,:) = [ 1.d0, -1.d0,  1.d0]
      b(63,:) = [-1.d0,  1.d0,  1.d0]
      b(64,:) = [ 1.d0,  1.d0,  1.d0]
      b(65,:) = [ 1.d0, -1.d0,  1.d0]
      b(66,:) = [ 1.d0,  1.d0, -1.d0]
      b(67,:) = [ 1.d0,  1.d0,  1.d0]
      b(68,:) = [-1.d0,  1.d0,  1.d0]
      b(69,:) = [ 1.d0,  1.d0, -1.d0]
      b(70,:) = [ 1.d0, -1.d0,  1.d0]
      b(71,:) = [-1.d0,  1.d0,  1.d0]
      b(72,:) = [ 1.d0,  1.d0,  1.d0]
      b(73,:) = [ 1.d0, -1.d0,  1.d0]
      b(74,:) = [ 1.d0,  1.d0, -1.d0]
      b(75,:) = [ 1.d0,  1.d0,  1.d0]
      b(76,:) = [-1.d0,  1.d0,  1.d0]
      b(77,:) = [-1.d0,  1.d0,  1.d0]
      b(78,:) = [ 1.d0,  1.d0,  1.d0]
      b(79,:) = [ 1.d0,  1.d0, -1.d0]
      b(80,:) = [ 1.d0, -1.d0,  1.d0]
      b(81,:) = [-1.d0,  1.d0,  1.d0]
      b(82,:) = [ 1.d0,  1.d0,  1.d0]
      b(83,:) = [ 1.d0,  1.d0, -1.d0]
      b(84,:) = [ 1.d0, -1.d0,  1.d0]
c
      n(1:12,:)  = n( 1:12,:)*inv3
      n(13:18,:) = n(13:18,:)*inv2
      n(25:36,:) = n(25:36,:)*inv6
      n(37:48,:) = n(37:48,:)*inv2
      n(49:60,:) = n(49:60,:)*inv6
      n(61:84,:) = n(61:84,:)*inv14
      b(1:36,:)  = b( 1:36,:)*inv2
      b(37:84,:) = b(37:84,:)*inv3
c
      do i=1, Nsizes
          call outer(b(i,:), n(i,:), 3, schmid(:,:,i))
          call getsym(schmid(:,:,i), 3, schmid_sym(:,:,i))
          call matrix2vec(schmid_sym(:,:,i), tmp)
          schmid_sym_vec(:,i) = tmp(2:)
          call getskw(schmid(:,:,i), 3, schmid_skw(:,:,i))
          schmid_skw_vec(:,i) = [schmid_skw(2,3,i),
     +                           schmid_skw(1,3,i),
     +                           schmid_skw(1,2,i)]
      end do
c
      k = 1
      r = 1
      do i=1, nssgr
          s = Nsg_temp(i)/2
          if (s .NE. 0) then
              P(:,k:k-1+s)         =  schmid_sym_vec(:,r:r-1+s)
              Omega(:,k:k-1+s)     =  schmid_skw_vec(:,r:r-1+s)
              if (full) then
                  P(:,k+s:k-1+2*s)     = -schmid_sym_vec(:,r:r-1+s)
                  Omega(:,k+s:k-1+2*s) = -schmid_skw_vec(:,r:r-1+s)
                  k = k + 2*s
              else
                  k = k + s
              end if
          end if
          r = r + sizes(i)/2
      end do
c
      if (full) then 
	      Nsg = Nsg_temp
	  else
	      Nsg = Nsg_temp/2
	  end if
      Nslips = sum(Nsg)
c
      return
      end subroutine init_slipsystems   
c   
c
      subroutine yldf(Nslips, full, S, crss, P, Nsmax, a, mode, 
     +                phi, dphi, ddphi, tau)
c
      implicit none
c     input
      integer :: Nslips, Nsmax, mode
      real(8) :: S(5), P(5, Nsmax), crss(Nslips), a
      logical :: full
c     output
      real(8) :: phi, dphi(5), ddphi(5,5), tau(Nslips)
c
      integer :: i
      real(8) :: tmp(Nslips), scale, tmp5(5,5), vdot
c     
      intent(in)  :: S, crss, P, mode, a, Nslips, Nsmax
      intent(out) :: phi, dphi, ddphi
c
      phi = 0.d0
c     scaling by the maximum element
      scale = 0.d0
      do i=1, Nslips
          if (full) then
              tau(i) = max(0.d0, vdot(S, P(:,i), 5))
          else 
              tau(i) = vdot(S, P(:,i), 5)
          end if
              
          tmp(i) = abs(tau(i))/crss(i)
          if (tmp(i) .GT. scale) scale = tmp(i)
      end do
c
      if (scale .LT. 1.d-8) then
          phi = 0.d0
          dphi = 0.d0
          ddphi = 0.d0
          return
      end if
c
      tmp = tmp/scale     
      do i=1, Nslips
          phi = phi + tmp(i)**a
      end do  
      phi = phi**(1.d0/a)
      phi = scale*phi
      
c     
c     mode = 0 only yield function is computed
      if (mode .EQ. 0) return
c     
      dphi = 0.d0
      do i=1, Nslips
          if (full) then
              dphi = dphi + P(:,i)/crss(i)*(tau(i)/(phi*crss(i)))
     +             **(a-1.d0) 
          else
              dphi = dphi + sign(1.d0,tau(i))*P(:,i)/crss(i)
     +             * (abs(tau(i))/(phi*crss(i)))**(a-1.d0)
          end if
      end do 
c
c     mode = 1 no hessian is computed
      if (mode .EQ. 1) return
c
c     calculate hessian
      ddphi = 0.d0
      do i=1, Nslips
          call outer(P(:,i), P(:,i), 5, tmp5)
          ddphi = ddphi + (1.d0/crss(i)**2)*(abs(tau(i))/(phi*crss(i)))
     +          **(a-2.d0) * tmp5
      end do
      call outer(dphi, dphi, 5, tmp5)
      ddphi = (a-1.d0)/phi * (ddphi - tmp5)
c
      return
      end subroutine yldf
c   
c 
      subroutine calc_slip(Nslips, tau, crss, phi, lamdot, a,
     +                     mode, gm, activesID, Nact)
c
      implicit none      
c     input
      integer :: Nslips, mode
      real(8) :: S(5), crss(Nslips), tau(Nslips), phi, 
     +           lamdot, a
c     output
      integer :: Nact, activesID(Nslips)
      real(8) :: gm(Nslips)
c
      integer :: i, j
c
      intent(in)  ::  Nslips, tau, crss, phi, lamdot, a, mode
      intent(out) ::  gm, activesID, Nact
    
c     compute gm
      do i=1, Nslips
          gm(i) = sign(1.d0, tau(i))*lamdot/crss(i)
     +          * (abs(tau(i))/(phi*crss(i)))**(a-1.d0)
      end do
c     calculate actives
c      j = 0
c      do i = 1, Nslips 
c          if (abs(gm(i)) > tol_act) then    tol_act NOT ASSIGNED
c              j = j + 1
c              activesID(j) = i
c          end if
c      end do
c      Nact = j   
      Nact = Nslips
      do i=1, Nslips
          activesID(i) = i
      end do
c
      return
      end subroutine calc_slip
c
c
      subroutine calc_h1(Nslips, Nsg, params, np, GAMMA, GM0, mod, crss, h)
c
      implicit none
c     input
      integer :: Nslips, np, mod, Nsg(7)
      real(8) :: GAMMA, GM0, params(np)
c     output  
      real(8) :: crss(Nslips), h(Nslips)
c
      integer :: i, gr, grp, ssum, tmp
c
      gr = 1 
c     find first nonzero slip group identificator
      do
          if (Nsg(gr) .NE. 0) exit
          gr = gr + 1
      end do
c      
      ssum = Nsg(gr)
      do i=1, Nslips
          if (i .GT. ssum) then
              do
                  gr = gr + 1
                  if (Nsg(gr) .NE. 0) exit
              end do
              ssum = ssum + Nsg(gr)
          end if
c
          grp = 1+8*gr     
c         unloading if GAMMA negative
          if (GAMMA .GE. 0.d0) then
              crss(i) = params(grp) 
     +                + params(grp+1)*(1.d0 - exp(-GAMMA/params(grp+2)))
     +                + params(grp+3)*(1.d0 - exp(-GAMMA/params(grp+4)))
     +                + params(grp+5)*(1.d0 - exp(-GAMMA/params(grp+6)))
     +                + params(grp+7)*GAMMA
          else
              tmp = params(grp+1)/params(grp+2)
     +            + params(grp+3)/params(grp+4)
     +            + params(grp+5)/params(grp+6)
     +            + params(grp+7)
              crss(i) = params(grp)*exp(GAMMA*tmp/params(grp))
          end if
          
          if (mod .EQ. 1) then
              if (GAMMA .GE. 0.d0) then
                  h(i) = params(grp+1)/params(grp+2)
     +                 * exp(-GAMMA/params(grp+2))
     +                 + params(grp+3)/params(grp+4)
     +                 * exp(-GAMMA/params(grp+4))
     +                 + params(grp+5)/params(grp+6)
     +                 * exp(-GAMMA/params(grp+6))
     +                 + params(grp+7)
              else
                  h(i) = tmp*exp(GAMMA*tmp/params(grp))
              end if
          end if
      end do
      return
      end subroutine calc_h1
c
c
      function vdot(x, y, n) result(v)
c          
      implicit none
      integer :: n, i
      real(8) :: x(n), y(n), v
c          
      v = 0.d0
      do i = 1, n
          v = v + x(i)*y(i)
      end do
c          
      return
      end function vdot
c
c
      function determ(A) result(DET)
c
      implicit none
      real(8) ::  A(3,3), DET
c         
      DET = A(1,1)*A(2,2)*A(3,3) +   
     +      A(2,1)*A(3,2)*A(1,3) + 
     +      A(3,1)*A(1,2)*A(2,3) - 
     +      A(3,1)*A(2,2)*A(1,3) - 
     +      A(1,1)*A(3,2)*A(2,3) - 
     +      A(2,1)*A(1,2)*A(3,3)
c
      return
      end function determ
c
c
      subroutine minv3(A)
c      
      implicit none
      integer ::  I,J
      real(8) ::  A(3,3), ADJ(3,3), DET, determ
c      
c     Compute the determinant of A
      DET = determ(A)
c     Compute the adjoint matrix of A
      ADJ(1,1)=A(2,2)*A(3,3)-A(3,2)*A(2,3)
      ADJ(1,2)=-(A(2,1)*A(3,3)-A(3,1)*A(2,3))
      ADJ(1,3)=A(2,1)*A(3,2)-A(3,1)*A(2,2)
      ADJ(2,1)=-(A(1,2)*A(3,3)-A(3,2)*A(1,3))
      ADJ(2,2)=A(1,1)*A(3,3)-A(3,1)*A(1,3)
      ADJ(2,3)=-(A(1,1)*A(3,2)-A(3,1)*A(1,2))
      ADJ(3,1)=A(1,2)*A(2,3)-A(2,2)*A(1,3)
      ADJ(3,2)=-(A(1,1)*A(2,3)-A(2,1)*A(1,3))
      ADJ(3,3)=A(1,1)*A(2,2)-A(2,1)*A(1,2)
c     Compute the transpose of the adjoint matrix of A = inverse of A
      do I=1,3
          do J=1,3
              A(I,J)=ADJ(J,I)/DET
          end do
      end do
c
      return
      end subroutine minv3
c
c      
      subroutine cross(x, y, z)
c
      implicit none
	real(8) :: x(3), y(3), z(3)
c        
	z(1) = x(2)*y(3)-y(2)*x(3)
	z(2) = x(3)*y(1)-y(3)*x(1)
	z(3) = x(1)*y(2)-y(1)*x(2)
c     
      return
      end subroutine cross
c
c
      function inner(A, B) result(c)
c        
      implicit none
      integer :: i, j
      real(8)	:: A(3,3), B(3,3), c
c          
      c = 0.d0
      do i = 1,3
          do j = 1,3
              c = c + A(i,j)*B(i,j)
          end do
      end do
c          
      return
      end function inner
c
c
      subroutine outer(x, y, n, A)
c
      implicit none
c
      integer :: i, j, n
      real(8)	:: x(n), y(n), A(n,n)
c
      A = 0.d0
      do i=1, n
          do j=1, n
              A(i,j) = x(i)*y(j)
          end do
      end do
c
      return
      end subroutine outer
c
c
      subroutine matrix2vec(A, v)
c     Transforms 3x3 matrix into new notation
      implicit none
c
      real(8) :: A(3,3), v(6)
c
	v(1) = 1.d0/sqrt(3.d0)*(A(1,1)+A(2,2)+A(3,3))
	v(2) = 1.d0/sqrt(6.d0)*(2.d0*A(3,3)-A(1,1)-A(2,2))
	v(3) = 1.d0/sqrt(2.d0)*(A(2,2)-A(1,1))
	v(4) = sqrt(2.d0)*A(2,3)
	v(5) = sqrt(2.d0)*A(1,3)
	v(6) = sqrt(2.d0)*A(1,2)
c
      return
      end subroutine matrix2vec
c
c
      subroutine vec2matrix(v, A)
c   
c     Transforms 6x1 vector v written in new notation
c     into a symmetric matrix A
c
      implicit none
c
      real(8) :: v(6), A(3,3)
c   
      A(1,1) = 1.d0/sqrt(3.d0)*v(1) - 1.d0/sqrt(6.d0)*v(2)
     +       - 1.d0/sqrt(2.d0)*v(3)
      A(2,2) = 1.d0/sqrt(3.d0)*v(1) - 1.d0/sqrt(6.d0)*v(2)
     +       + 1.d0/sqrt(2.d0)*v(3)
      A(3,3) = 1.d0/sqrt(3.d0)*v(1) + sqrt(2.d0/3.d0)*v(2)
      A(2,3) = 1.d0/sqrt(2.d0)*v(4)
      A(1,3) = 1.d0/sqrt(2.d0)*v(5)
      A(1,2) = 1.d0/sqrt(2.d0)*v(6)
      A(3,2) = A(2,3)
      A(3,1) = A(1,3)
      A(2,1) = A(1,2)
c
      return
      end subroutine vec2matrix
c
c
      subroutine matrix2vecABAQUS(A, v, mod)
c     Transforms 3x3 matrix into Voigt notation
c     of Abaqus/Standard
      implicit none
c
      real(8) :: A(3,3), v(6), f
      character(1) :: mod
c
	v(1) = A(1,1)
	v(2) = A(2,2)
	v(3) = A(3,3)
      f = 1.d0
      if (mod .EQ. 'D') f = 2.d0
	v(4) = f*A(1,2)
	v(5) = f*A(1,3)
	v(6) = f*A(2,3)
c
      return
      end subroutine matrix2vecABAQUS
c
c
      subroutine vecABAQUS2matrix(v, A, mod)
c     Transforms 6x1 vector v written in Voigt notation
c     of Abaqus/Standard into a symmetric matrix A
      implicit none
c
      real(8) :: v(6), A(3,3), f
      character(1) :: mod
c   
      A(1,1) = v(1)
      A(2,2) = v(2)
      A(3,3) = v(3)
      f = 1.d0
      if (mod .EQ. 'D') f = 0.5d0
      A(2,3) = f*v(6)
      A(1,3) = f*v(5)
      A(1,2) = f*v(4)
      A(3,2) = A(2,3)
      A(3,1) = A(1,3)
      A(2,1) = A(1,2)
c
      return
      end subroutine vecABAQUS2matrix
c
c
      function deg2rad(angd) result(angr)
c          
      implicit none
      real(8) :: angd, angr
c          
      angr = 0.01745329251d0*angd
c      
      return     
      end function deg2rad 
c
c           
      function rad2deg(angr) result(angd)
c     
      implicit none
      real(8) :: angd, angr
c          
      angd = 57.2957795131d0*angr
c      
      return
      end function rad2deg
c
c
      subroutine euler2rotm(ANG, Q0)
c     ANG three Euler angles in degrees
      implicit none
      real(8) :: ANG(3), Q0(3,3), ANG1, ANG2, ANG3, deg2rad

      ANG1 = deg2rad(ANG(1))
      ANG2 = deg2rad(ANG(2))
      ANG3 = deg2rad(ANG(3))

      Q0(1,1) = COS(ANG1)*COS(ANG3)-SIN(ANG1)*COS(ANG2)*SIN(ANG3)
      Q0(1,2) = SIN(ANG1)*COS(ANG3)+COS(ANG1)*COS(ANG2)*SIN(ANG3)
      Q0(1,3) = SIN(ANG2)*SIN(ANG3)
      Q0(2,1) =-COS(ANG1)*SIN(ANG3)-SIN(ANG1)*COS(ANG2)*COS(ANG3)
      Q0(2,2) =-SIN(ANG1)*SIN(ANG3)+COS(ANG1)*COS(ANG2)*COS(ANG3)
      Q0(2,3) = SIN(ANG2)*COS(ANG3)
      Q0(3,1) = SIN(ANG1)*SIN(ANG2)
      Q0(3,2) =-COS(ANG1)*SIN(ANG2)
      Q0(3,3) = COS(ANG2)
c
      return
      end subroutine euler2rotm
c
c
      subroutine rotm2euler(Q, ANG)
c
      implicit none
      real(8) :: ANG(3), Q(3,3), ANG1, ANG2, ANG3, STH, rad2deg

      if (abs(Q(3,3)) < 1.D0) then
        ANG2 = acos(Q(3,3))
        STH  = sin(ANG2)
        ANG1 = atan2(Q(3,1)/STH,-Q(3,2)/STH)
        ANG3 = atan2(Q(1,3)/STH,Q(2,3)/STH)
      else
        ANG1 = atan2(Q(1,2),Q(1,1))
        ANG2 = 0.D0
        ANG3 = 0.D0
      end if
      ANG(1) = rad2deg(ANG1)
      ANG(2) = rad2deg(ANG2)
      ANG(3) = rad2deg(ANG3)
c
      return
      end subroutine rotm2euler     
c
c
      SUBROUTINE DEC (N, NDIM, A, IP, IER)
C VERSION REAL DOUBLE PRECISION
      INTEGER N,NDIM,IP,IER,NM1,K,KP1,M,I,J
      DOUBLE PRECISION A,T
      DIMENSION A(NDIM,N), IP(N)
C-----------------------------------------------------------------------
C  MATRIX TRIANGULARIZATION BY GAUSSIAN ELIMINATION.
C  INPUT..
C     N = ORDER OF MATRIX.
C     NDIM = DECLARED DIMENSION OF ARRAY  A .
C     A = MATRIX TO BE TRIANGULARIZED.
C  OUTPUT..
C     A(I,J), I.LE.J = UPPER TRIANGULAR FACTOR, U .
C     A(I,J), I.GT.J = MULTIPLIERS = LOWER TRIANGULAR FACTOR, I - L.
C     IP(K), K.LT.N = INDEX OF K-TH PIVOT ROW.
C     IP(N) = (-1)**(NUMBER OF INTERCHANGES) OR O .
C     IER = 0 IF MATRIX A IS NONSINGULAR, OR K IF FOUND TO BE
C           SINGULAR AT STAGE K.
C  USE  SOL  TO OBTAIN SOLUTION OF LINEAR SYSTEM.
C  DETERM(A) = IP(N)*A(1,1)*A(2,2)*...*A(N,N).
C  IF IP(N)=O, A IS SINGULAR, SOL WILL DIVIDE BY ZERO.
C
C  REFERENCE..
C     C. B. MOLER, ALGORITHM 423, LINEAR EQUATION SOLVER,
C     C.A.C.M. 15 (1972), P. 274.
C-----------------------------------------------------------------------
      IER = 0
      IP(N) = 1
      IF (N .EQ. 1) GO TO 70
      NM1 = N - 1
      DO 60 K = 1,NM1
        KP1 = K + 1
        M = K
        DO 10 I = KP1,N
          IF (DABS(A(I,K)) .GT. DABS(A(M,K))) M = I  
 10     CONTINUE
        IP(K) = M
        T = A(M,K)
        IF (M .EQ. K) GO TO 20
        IP(N) = -IP(N)
        A(M,K) = A(K,K)
        A(K,K) = T
 20     CONTINUE
        IF (T .EQ. 0.D0) GO TO 80
        T = 1.D0/T
        DO 30 I = KP1,N
 30       A(I,K) = -A(I,K)*T
        DO 50 J = KP1,N
          T = A(M,J)
          A(M,J) = A(K,J)
          A(K,J) = T
          IF (T .EQ. 0.D0) GO TO 45
          DO 40 I = KP1,N
 40         A(I,J) = A(I,J) + A(I,K)*T
 45       CONTINUE
 50       CONTINUE
 60     CONTINUE
 70   K = N
      IF (A(N,N) .EQ. 0.D0) GO TO 80
      RETURN
 80   IER = K
      IP(N) = 0
      RETURN
      END
C
C
      SUBROUTINE SOL (N, NDIM, A, B, IP)
C VERSION REAL DOUBLE PRECISION
      INTEGER N,NDIM,IP,NM1,K,KP1,M,I,KB,KM1
      DOUBLE PRECISION A,B,T
      DIMENSION A(NDIM,N), B(N), IP(N)
C-----------------------------------------------------------------------
C  SOLUTION OF LINEAR SYSTEM, A*X = B .
C  INPUT..
C    N = ORDER OF MATRIX.
C    NDIM = DECLARED DIMENSION OF ARRAY  A .
C    A = TRIANGULARIZED MATRIX OBTAINED FROM DEC.
C    B = RIGHT HAND SIDE VECTOR.
C    IP = PIVOT VECTOR OBTAINED FROM DEC.
C  DO NOT USE IF DEC HAS SET IER .NE. 0.
C  OUTPUT..
C    B = SOLUTION VECTOR, X .
C-----------------------------------------------------------------------
      IF (N .EQ. 1) GO TO 50
      NM1 = N - 1
      DO 20 K = 1,NM1
        KP1 = K + 1
        M = IP(K)
        T = B(M)
        B(M) = B(K)
        B(K) = T
        DO 10 I = KP1,N
 10       B(I) = B(I) + A(I,K)*T
 20     CONTINUE
      DO 40 KB = 1,NM1
        KM1 = N - KB
        K = KM1 + 1
        B(K) = B(K)/A(K,K)
        T = -B(K)
        DO 30 I = 1,KM1
 30       B(I) = B(I) + A(I,K)*T
 40     CONTINUE
 50   B(1) = B(1)/A(1,1)
      RETURN
      END
c
c
      subroutine read_trial(ifile, list)

      implicit none
      integer             ::  FID, I, J, ALIVE
      real(8)             ::  list(10000,5)
      character(12)       ::  ifile
      
      FID=1112
      open(FID,FILE=ifile, STATUS='OLD', IOSTAT=ALIVE)   
      
      do I=1,10000
          read(FID,*) (list(I,J), J=1,5)
      end do
      close(FID)

      return
      end subroutine read_trial   
c
c
      subroutine chol_decomp(A, n, choldiag)
c**********************************************************************
c   The CHOL_DECOMP function calculates the Cholesky decomposition
c   A = L*LT of matrix A, where L is lower-triangular matrix and is
c   stored in the lower triangle of A except for the diagonal, which
c   is stored in array CHOLDIAG
c
c**********************************************************************
c (IN/OUT) A is REAL*8 array, dimension (n,n)
c          Matrix A must be a positive-definite symmetric matrix and
c          at input it is read from the upper triangle of A. At return,
c          the factorized matrix L will be stored in the lower triangle
c          of A.
c (IN)     N is INTEGER,
c          It is the dimension of A.
c (OUT)    CHOLDIAG is REAL*8 array, dimension (n)
c          It contains diagonal Cholesky factors of matrix A. 
c
      implicit none
      integer     i, j, k, n
      real*8      A(n,n), choldiag(n), sum
c
c     Cholesky factorization of A
      do i=1, n
         do j=i, n
            sum = A(i,j)
            do k=i-1, 1, -1
               sum = sum - A(i,k)*A(j,k)
            end do
            if (i .EQ. j) then
               choldiag(i) = sqrt(sum)
            else
               A(j,i) = sum/choldiag(i)
            end if
         end do
      end do
c 
      return
      end subroutine chol_decomp
c
c
      subroutine chol_solve(A, n, b, choldiag, x)
c**********************************************************************
c      The CHOL_SOLVE function returns an n-element vector X containing 
c      the solution to the set of linear equations Ax = b. The Cholesky
c      factorization of A must be stored in the lower-diagonal of A and
c      the diagonal factors in CHOLDIAG.
c
c**********************************************************************
c (IN)     A is REAL*8 array, dimension (n,n)
c          Array A contains the original matrix A stored in the upper 
c          triangle including the diagonal. The Cholesky factorization
c          of A must at input be stored in the lower triangle, and
c          the diagonal factors stored in CHOLDIAG.
c (IN)     N is INTEGER,
c          It is the dimension of the linear system Ax = b
c (IN)     B is REAL*8 array, dimension (n)
c          It is the right-hand-side vector of the linear system Ax = b
c (IN)     CHOLDIAG is REAL*8 array, dimension (n)
c          It contains diagonal Cholesky factors of matrix A.
c (OUT)    X is REAL*8, dimension (n)
c          It is the solution of the linear system Ax = b
c
      implicit none
      integer     i, j, k, n
      real*8      A(n,n), b(n), choldiag(n), x(n), sum
c
c     backsubstitution
      do i=1, n
         sum = b(i)
         do k=i-1, 1, -1
            sum = sum - A(i,k)*x(k)
         end do
         x(i) = sum/choldiag(i)
      end do
c
      do i=n, 1, -1
         sum = x(i)
         do k=i+1, n
            sum = sum - A(k,i)*x(k)
         end do
         x(i) = sum/choldiag(i)
      end do

      return
      end subroutine chol_solve
c
c
      subroutine chol_inverse(A, n, choldiag, C)
c**********************************************************************
c      The CHOL_INVERSE function returns the matrix C which is inverse of 
c      a positive-definite symmetric matrix A.
c
c**********************************************************************
c (IN)     A is REAL*8 array, dimension (n,n)
c          Array A contains the original matrix A stored in the upper 
c          triangle including the diagonal. The Cholesky factorization
c          of A must at input be stored in the lower triangle, and
c          the diagonal factors stored in CHOLDIAG.
c (IN)     N is INTEGER,
c          It is the dimension of the linear system Ax = b
c (IN)     B is REAL*8 array, dimension (n)
c          It is the right-hand-side vector of the linear system Ax = b
c (IN)     CHOLDIAG is REAL*8 array, dimension (n)
c          It contains diagonal Cholesky factors of matrix A.
c (OUT)    C is REAL*8, dimension (n,n)
c          It contains inverse of A
c
      implicit none
      integer     i, j, k, n, m
      real*8      A(n,n), b(n), choldiag(n), sum, C(n,n)
c
c     inverse of A by factorized A stored in lower
c     triangle of A and in CHOLDIAG
      do m=1, n
c         create the cartesian basis vectors b    
          do i=1, n
              if (m .EQ. i) then
                  b(i) = 1.d0
              else
                  b(i) = 0.d0
              end if
          end do
c         fill the columns of C with the solutions x=A-1*b
          do i=1, n
             sum = b(i)
             do k=i-1, 1, -1
                sum = sum - A(i,k)*C(k,m)
             end do
             C(i,m) = sum/choldiag(i)
          end do
c
          do i=n, 1, -1
             sum = C(i,m)
             do k=i+1, n
                sum = sum - A(k,i)*C(k,m)
             end do
             C(i,m) = sum/choldiag(i)
          end do
      end do
c      
      return
      end subroutine chol_inverse    
c      
c
      subroutine minalg(x, y, xL, xR, yL0, eps, eps0, status, wa)
c     beste Bjorn sin publication version
c     recommended:
c     eps0=1.d0-2*eps, eps=min(0.03,1.d0/n), n=YS exp.
c     input: x,y (latest point calculated in calling program)
c     output x, an updated new point to be calculated
c     .      xnew, newest estimate for x, saved
c     eps0,eps: absolute and relative (machine) error
c     status: returns 0 when converged, initial 0
c     xL, xR : trust boundaries. updated during iterations 
c
      implicit none
      integer :: j,status, iflag
      real(8) :: xjm2,xjm1,xj,x,yjm2,yjm1,yj,y
      real(8) :: xL,yL,xM,yM,xR,yR, xnew,xold,xolder,yL0
      real(8) :: eps, eps0, gld1,gld2,xtmp
      real(8) :: tol(2),cratio,ynew,yold
      real(8) :: wa(15)
c      save xjm2,xjm1,xj,yjm2,yjm1,yj
c      save xM,yM,yL,yR,xnew,xold,xolder,ynew,yold
c
      xjm2 = wa(1)
      xjm1 = wa(2)
      xj   = wa(3)
      yjm2 = wa(4)
      yjm1 = wa(5)
      yj   = wa(6)
c
      xM     = wa(7)
	yM     = wa(8)
	yL     = wa(9) 
	yR     = wa(10) 
	xnew   = wa(11) 
	xold   = wa(12) 
	xolder = wa(13)  
	ynew   = wa(14) 
	yold   = wa(15) 
c
c     first call
c      xL=max(xL,1.d-7)
      gld2 = 0.62d0
      gld1 = 1.d0 - gld2
      if (status.eq.0) then
         x =xR
         xj=0.
         yj=yL0
         xold=xL
         yold=yL0
         xnew    = x
         yL      = yL0
         status  = status + 1	 
		 wa = [xjm2,xjm1,xj,yjm2,yjm1,yj,
     +         xM,yM,yL,yR,xnew,xold,xolder,ynew,yold]
         return
      endif
c     second call
      if (status.eq.1) then
         If(y.lt.yL0*0.99d0) then
            status=0
			wa = [xjm2,xjm1,xj,yjm2,yjm1,yj,
     +            xM,yM,yL,yR,xnew,xold,xolder,ynew,yold]
            Return
         endif
         yR      = y
         xjm1    = xj
         yjm1    = yj
         xj      = x
         yj      = y
c     quadratic guess ala Schertzinger
         x       = max(xR*xR*yL0/(yL0*(2.d0*xR-1.d0)+y),0.1*xR)
c         x       = yL0/(yL0+y)
         xold    = xnew
         xnew    = x
         yold    = y
         status  = status + 1
         tol(1)  = xnew
         tol(2)  = abs(xnew-xj)
		 wa = [xjm2,xjm1,xj,yjm2,yjm1,yj,
     +         xM,yM,yL,yR,xnew,xold,xolder,ynew,yold]
         return
      endif
c     cases with status .ge. 2
c     update the ordering of the points
c     set a safe midpoint for status  =2
      if (status.eq.2) then
         if(y.lt.min(yL,yR)) then
            xM = x
            yM = y
         else
            if(yL.lt.yR) then
               xM=xL
               yM=yL
            else
               xM=xR
               yM=yR
            endif
         endif
      endif
c      if (status.ge.3) then
         xjm2 = xjm1
         yjm2 = yjm1
c      endif
      xjm1 = xj
      yjm1 = yj
      xj   = x
      yj   = y
c     first check if trust boundaries can immediately be moved
      if (y.gt.yL) then
         xR = xj
         yR = yj
         goto 2000
      endif
c     move left boundary only if it increases yL
      if ((y.gt.yR).and.(y.lt.yL)) then
         xL = xj
         yL = yj
         goto 2000
      endif
c     from here we know that yM.le.min(yL,yR)
c     and that x.neq.xM
c     we analyse based on x and xM, which trust border to update 
c     
      if(x.gt.xM) then
         if(y.gt.yM)then
            xR = xj
            yR = yj
         endif
         if(y.lt.yM)then
            xL = xM
            yL = yM
            xM = xj
            yM = yj
         endif
      endif
      if(x.lt.xM) then
         if(y.gt.yM) then
            xL = xj
            yL = yj
         endif
         if(y.lt.yM) then
            xR = xM
            yR = yM
            xM = xj
            yM = yj
         endif
      endif
	  wa = [xjm2,xjm1,xj,yjm2,yjm1,yj,
     +      xM,yM,yL,yR,xnew,xold,xolder,ynew,yold]
c     return point for next iteration
 2000 continue
c     try cubic polynomial first for xnew
c
      xolder = xold
      xold   = xnew
c
c      if(status.eq.2) goto 477
c
      call xmin(xjm2,xjm1,xj,yjm2,yjm1,yj,xnew,iflag)
c     
c     make a golden section choise based on trust region
c     when the polynomial fails
      if(iflag.ne.1) goto 477
      if((xnew.ge.xR).or.(xnew.le.xL)) goto 477
      yold   = ynew
      call ymin(xjm2,xjm1,xj,yjm2,yjm1,yj,xnew,ynew)
c      if(ynew.gt.yL0) goto 477
      tol(1) = xnew
      tol(2) = 0.5d0*(abs(xnew - xold) + abs(xjm1-xjm2))
      If((tol(2).lt.(eps*abs(xnew) + eps0)).and.(ynew.ge.yL0)) goto 477
c     
c     golden section when the polynomial guess converges too slow 
      cratio=abs((xnew-xold)/(xold-xolder))
      If(cratio.gt.0.62d0) goto 477
c     
c     polynomial guess is within the trust region:
c     decide the next point, x, to be evaluated
c      xtmp = xj + 2.d0*(xnew-xj)
      xtmp = xj + 1.5d0*(xnew-xj)
      if((xtmp.le.xR).and.(xtmp.ge.xL))then
         x = xtmp
      else
         x = xnew
      endif
c     
      goto 478
c
c     GOLDEN 
 477  continue
c      write(*,*) "golden"
c     at this point we need to make a golden section guess
      if((xR-xM).gt.(xM-xL))then
         xnew = gld2*xM + gld1*xR
      else
         xnew = gld1*xL + gld2*xM
      endif
      x = xnew
c     here yold is kept and the previous y is used for the tol_y
      ynew = y
c
 478  continue

c     AT THIS POINT, xnew and x are determined
c     check for convergence
      status = status+1
c
      tol(1) = xnew
c      tol(2) = abs(xnew - xold)
      tol(2) = 0.5d0*(abs(xnew - xold) + abs(xjm1-xjm2))
c
      If(tol(2).lt.(eps*abs(xnew) + eps0)) then
         x      = xnew
         status = 0
      endif
c
      wa = [xjm2,xjm1,xj,yjm2,yjm1,yj,
     +      xM,yM,yL,yR,xnew,xold,xolder,ynew,yold]
      return
      end subroutine minalg
c
c
      subroutine xmin(xjm2,xjm1,xj,yjm2,yjm1,yj,minx,iflag)
c     iflag=1 if successfull, otherwise 0
      implicit none
      integer :: iflag
      real(8) :: minx,xjm2,xjm1,xj,yjm2,yjm1,yj
      real(8) :: tmp1, ydd
      if(abs(xj-xjm2).lt.1.0d-20) then
         iflag=0
         return
      endif
      ydd=((yjm1-yjm2)/(xjm1-xjm2) - (yj-yjm2)/(xj-xjm2))/(xjm1-xj)
      if(ydd.le.0.d0) then
         iflag=0
         return
      endif
      tmp1=((yjm2-yj)*(xjm1-xj)-(yjm1-yj)*(xjm2-xj))
      minx =(yjm2-yj)*(xjm1-xjm2)*(xjm1-xj)     
      if((abs(tmp1).lt.1.d-10*minx).or.abs(tmp1).lt.1.d-20) then
         iflag=0
         return
      endif
      minx =xjm2+xj+minx/tmp1
      minx=0.5d0*minx
      iflag=1
      return
      end subroutine xmin
c
      subroutine machepsd(eps)
c     calculate the relative machine constant
      implicit none
      real(8) :: eps, tmp
      eps=1.d0
 111  continue
      if((1.d0+0.5d0*eps).ne.1.d0) then
         eps=0.5d0*eps
         goto 111
      endif
      return
      end subroutine machepsd

c
      subroutine ymin(xjm2,xjm1,xj,yjm2,yjm1,yj,x,y)
c     iflag=1 if successfull, otherwise 0
      implicit none
      real(8) :: x,y,xjm2,xjm1,xj,yjm2,yjm1,yj
      real(8) :: tmp
      y = yj*(x-xjm1)*(x-xjm2)/(xj-xjm1)/(xj-xjm2)
      y = y + yjm1*(x-xj)*(x-xjm2)/(xjm1-xj)/(xjm1-xjm2)
      y = y + yjm2*(x-xj)*(x-xjm1)/(xjm2-xj)/(xjm2-xjm1)      
      return
      end subroutine ymin
c
c     
      subroutine get_rot5(R, Q)
c
      implicit none
      real(8) :: R(3,3), Q(5,5)
c
      Q(1,1) = (3.d0*R(3,3)**2-1.d0)*0.5d0
      Q(1,2) = sqrt(3.d0)*(R(3,2)**2-R(3,1)**2)*0.5d0
      Q(1,3) = sqrt(3.d0)*R(3,2)*R(3,3)
      Q(1,4) = sqrt(3.d0)*R(3,1)*R(3,3)
      Q(1,5) = sqrt(3.d0)*R(3,1)*R(3,2)
      Q(2,1) = sqrt(3.d0)*(R(2,3)**2-R(1,3)**2)*0.5d0
      Q(2,2) = (R(1,1)**2+R(2,2)**2-R(1,2)**2-R(2,1)**2)*0.5d0
      Q(2,3) = R(2,2)*R(2,3)-R(1,2)*R(1,3)
      Q(2,4) = R(2,3)*R(2,1)-R(1,3)*R(1,1)
      Q(2,5) = R(2,1)*R(2,2)-R(1,1)*R(1,2)
      Q(3,1) = sqrt(3.d0)*R(2,3)*R(3,3)
      Q(3,2) = R(2,2)*R(3,2)-R(2,1)*R(3,1)
      Q(3,3) = R(2,2)*R(3,3)+R(2,3)*R(3,2)
      Q(3,4) = R(2,1)*R(3,3)+R(2,3)*R(3,1)
      Q(3,5) = R(2,2)*R(3,1)+R(2,1)*R(3,2)
      Q(4,1) = sqrt(3.d0)*R(1,3)*R(3,3)
      Q(4,2) = R(3,2)*R(1,2)-R(3,1)*R(1,1)
      Q(4,3) = R(1,2)*R(3,3)+R(1,3)*R(3,2)
      Q(4,4) = R(1,1)*R(3,3)+R(1,3)*R(3,1)
      Q(4,5) = R(1,2)*R(3,1)+R(1,1)*R(3,2)
      Q(5,1) = sqrt(3.d0)*R(1,3)*R(2,3)
      Q(5,2) = R(1,2)*R(2,2)-R(1,1)*R(2,1)
      Q(5,3) = R(1,2)*R(2,3)+R(1,3)*R(2,2)
      Q(5,4) = R(1,1)*R(2,3)+R(1,3)*R(2,1)
      Q(5,5) = R(1,1)*R(2,2)+R(1,2)*R(2,1)
c
      return
      end subroutine get_rot5
c
c
      SUBROUTINE MVMULT(A,V,N,Z)
C
      IMPLICIT NONE
C
      REAL*8 A(N,N),Z(N),V(N),P
      INTEGER I,K,N
C
      DO I=1,N
         P=0.D0
         DO K=1,N
            P=P+A(I,K)*V(K)
         ENDDO
         Z(I)=P
      ENDDO
C
      RETURN
      END SUBROUTINE MVMULT
c
c
      SUBROUTINE MMULT(A,B,N,AB)
C
      IMPLICIT NONE
C
      REAL*8 A(N,N),B(N,N),AB(N,N),P
      INTEGER I,J,K,N
C
      DO I=1,N
         DO J=1,N
            P=0.D0
            DO K=1,N
               P=P+A(I,K)*B(K,J)
            ENDDO
            AB(I,J)=P
         ENDDO
      ENDDO
C
      RETURN
      END SUBROUTINE MMULT
c
c
      SUBROUTINE MTRANSP(A,N,AT)
C
      IMPLICIT NONE
C
      REAL*8 A(N,N),AT(N,N)
      INTEGER I,J,N
C
      DO I=1,N
         DO J=1,N
            AT(I,J)=A(J,I)
         ENDDO
      ENDDO
C
      RETURN
      END SUBROUTINE MTRANSP
c
c
      SUBROUTINE TRANSFORM(A,P,N,B)
C     Rotates a matrix from one frame to another
C     B = P^T.A.P
      IMPLICIT NONE
C
      REAL*8 A(N,N),B(N,N),P(N,N),PT(N,N),MAT(N,N)
      INTEGER N
C     
      CALL MTRANSP(P,N,PT)
      CALL MMULT(A,P,N,MAT)
      CALL MMULT(PT,MAT,N,B)
C
      RETURN
      END SUBROUTINE TRANSFORM
c
c
      SUBROUTINE TRANSFORMB(A,P,N,B)
C     Rotates a matrix from one frame to another
C     B = P.A.P^T
      IMPLICIT NONE
C     
      REAL*8 A(N,N),B(N,N),P(N,N),PT(N,N),MAT(N,N)
      INTEGER N
C     
      CALL MTRANSP(P,N,PT)
      CALL MMULT(A,PT,N,MAT)
      CALL MMULT(P,MAT,N,B)
C
      RETURN
      END SUBROUTINE TRANSFORMB
c
c
      subroutine getsym(A, N, ASYM)
c
      implicit none
c
      real(8) :: A(N,N), ASYM(N,N)
	  integer :: i, j, N
c
      do i=1,N
	     do j=1,N
		    ASYM(i,j) = 0.5d0*(A(i,j)+A(j,i))
		 end do
      end do
c
      return
      end subroutine getsym
c
c
      subroutine getskw(A, N, ASKW)
c
      implicit none
c
      real(8) :: A(N,N), ASKW(N,N)
	  integer :: i, j, N
c
      do i=1,N
	     do j=1,N
		    ASKW(i,j) = 0.5d0*(A(i,j)-A(j,i))
		 end do
      end do
c
      return
      end subroutine getskw